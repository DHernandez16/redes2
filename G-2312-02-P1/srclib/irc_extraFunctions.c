/**
 * @file irc_extraFuntions.h 
 * @brief general extra Funcions to help
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "irc_extraFunctions.h"

char * strclone(char * str){
    int dim;
    char * out;
    if(str == NULL){
        return NULL;
    }

    dim = strlen(str) + 1;
    if((out = (char *) calloc(sizeof(char), dim)) == NULL){
        return NULL;
    }

    strcpy(out, str);
    return out;
}

void printMsg(char *msg, int log_mode, int perr) {
    if (msg != NULL) {
    #ifdef DEBUG_PRINT
            if (perr) {
                perror(msg);
            } else {
                fprintf(stdout, "%s", msg);
            }
    #else
            syslog(log_mode, "%s", msg);
    #endif
    }
}

int SendNEEDMOREPARAMS(char * str, DataUser * dataUser){
    char *command = NULL;
    if(IRCMsg_ErrNeedMoreParams (&command, dataUser->prefix, dataUser->nick, str) != IRC_OK){
        return -1;
    }

    write(dataUser->socket , command , strlen(command));
    return 0;
}

int SendUNKNOWNCOMMAND(char * str, DataUser * dataUser){
    char *command = NULL;
    if(IRCMsg_ErrUnKnownCommand (&command, dataUser->prefix, dataUser->nick, str) != IRC_OK){
        return -1;
    }

    write(dataUser->socket , command , strlen(command));
    return 0;
}

int getPosOfChanel(char **listchannels,long numberOfChannels, char * channel){
    int i;
    for(i=0; i< numberOfChannels; i++){
        if (strcmp(channel, listchannels[i]) == 0){
            return i;
        }
    }
    return -1;
}

int getPosOfUser(char **listusers,long numberOfUsers, char * user){
    int i;
    for(i=0; i< numberOfUsers; i++){
        if (strcmp(user, listusers[i]) == 0){
            return i;
        }
    }
    return -1;
}
