/**
 * @file commands.c
 * @brief Functions to manage commands
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "commands.h"

DataUser *regUser(int socket) {
    /*Variables de lectura*/
    int bytesread; /*Bytes leidos*/
    char str_read[BYTES_READ]; /*Cadena para leer mensaje*/
    char* string_command; /*cadena con el comando a analizar*/
    char *str_aux = NULL; /*Cadena para partir comandos aun no analizados*/
    long comm_ret; /*Tipo de comando a parsear*/

    /*Flag registo*/
    int flag_nick = 0;
    int flag_user = 0;
    long return_NewUser;
    DataUser *dataUser = NULL;


    /*Variables parseo*/
    char *prefix = NULL;
    char *nick = NULL;
    char *msg = NULL;
    char *user = NULL;
    char *modehost = NULL;
    char *serverother = NULL;
    char *realname = NULL;
    char *password = NULL;
    long returnCode;

    /*Mensajes a enviar*/
    char * msgCommand = NULL;  

    /*CODIGO*/

    if((dataUser = (DataUser*) calloc(sizeof(DataUser), 1)) == NULL){
        return NULL;
    }

    dataUser->user = user;
    dataUser->nick = NULL;
    dataUser->realname = NULL;
    dataUser->password = NULL;
    dataUser->host = NULL;
    dataUser->IP = NULL;        
    dataUser->prefix = NULL;
    dataUser->listChannelsCreates = NULL;
    dataUser->socket = -1;

    /*Registro de usuario*/
    while (1) {
        /*Leer un comando*/
        bzero(str_read, BYTES_READ);
        bytesread = read(socket, str_read, BYTES_READ);
        if (bytesread == 0) {
            freeDataUser(dataUser);
            return NULL;
        }

        str_aux = str_read; /*Valor inicial de str_aux mensaje recibido*/
        do {/*Sacar comandos de un mensaje*/
            /*str_aux resto de mensajes sin analizar*/
            str_aux = IRC_UnPipelineCommands(str_aux, &string_command); /*string_command = COMMAND string, unpiped_string = resto de cadena*/

            /*Get ID del comando*/
            comm_ret = IRC_CommandQuery(string_command);
            /*Case segun el tipo*/
            switch (comm_ret) {
                case USER:
                    /*Sacar Datos*/
                    returnCode = IRCParse_User(string_command, &prefix, &user, &modehost, &serverother, &realname);

                    /*Liberar parametros que no nos interesan*/
                    free(prefix);
                    prefix = NULL;
                    free(modehost);
                    modehost = NULL;
                    free(serverother);
                    serverother = NULL;

                    if (returnCode != IRC_OK) { /*Error*/
                        free(user);
                        user = NULL;
                        free(realname);
                        realname = NULL;
                    } else {
                        flag_user = 1;
                    }
                    break;

                case NICK:
                    /*Sacar Datos*/
                    returnCode = IRCParse_Nick(string_command, &prefix, &nick, &msg);

                    /*Liberar parametros que no nos interesan*/
                    free(prefix);
                    prefix = NULL;
                    free(msg);
                    msg = NULL;
                    if (returnCode != IRC_OK) { /*Error*/
                        free(nick);
                        nick = NULL;
                    } else {
                        flag_nick = 1;
                    }
                    break;

                case PASS:
                    /*PASS Tiene que venir primero*/
                    if (flag_user) {
                        free(user);
                        user = NULL;
                        free(realname);
                        realname = NULL;
                        free(serverother); /*Host*/
                        serverother = NULL;
                        flag_user = 0;
                    }

                    if (flag_nick) {
                        free(nick);
                        nick = NULL;
                        flag_nick = 0;
                    }


                    returnCode = IRCParse_Pass(string_command, &prefix, &password);
                    if (returnCode != IRC_OK) { /*Error*/
                        free(password);
                        password = NULL;
                    }
                    /*Liberar parametros que no nos interesan*/
                    free(prefix);
                    prefix = NULL;
                    break;

                default:
                    /*Empezar de 0 si no ha recibido el nick o el user*/
                    if(flag_nick == 0 || flag_user == 0){
                        flag_nick = 0;
                        flag_user = 0;

                        /*Liberar todo, volvemos a empezar*/
                        free(nick);
                        nick = NULL;
                        free(user);
                        user = NULL;
                        free(realname);
                        realname = NULL;
                        free(serverother); /*Host*/
                        serverother = NULL;
                        free(password);
                        password = NULL;
                    }
                    break;
            }

            free(string_command);
            string_command = NULL;

        } while (str_aux != NULL);

        if (flag_user && flag_nick) {
            /*Creamos la estructura de datos del usuario*/
            dataUser->user = user;
            dataUser->nick = nick;
            dataUser->realname = realname;
            dataUser->password = password;
            //dataUser->IP = strclone(IP);
            struct sockaddr_in addr;
            socklen_t size_addr = sizeof(addr);
            getpeername(socket, (struct sockaddr *) &addr, &size_addr);
            dataUser->IP = strclone(inet_ntoa(addr.sin_addr)); 
            // printMsg(dataUser->IP, LOG_DEBUG, 0);
            struct hostent * he;
            he = gethostbyaddr((char *) &addr.sin_addr, sizeof(addr.sin_addr), AF_INET); 

            if(he == NULL){
                herror("Error to get namehost\n");
                freeDataUser(dataUser);
                return NULL;
            }else{
                dataUser->host = strclone(he->h_name);
            }

            /*Crear Prefix*/
            if(IRC_Prefix (&(dataUser->prefix), dataUser->nick, dataUser->user, dataUser->host, "server.irc.redesii") != IRC_OK){
                freeDataUser(dataUser);
                return NULL;
            }
            //printMsg(dataUser->prefix, LOG_DEBUG, 0);
            //printMsg("\n", LOG_DEBUG, 0);
            dataUser->socket = socket;

            if((dataUser->listChannelsCreates  = (NodeListChannels *) calloc (sizeof (NodeListChannels), 1)) != NULL){
                dataUser->listChannelsCreates->channel = strclone("Inicio Channles");
                dataUser->listChannelsCreates->next = NULL;
            }

            return_NewUser = IRCTADUser_New(dataUser->user, dataUser->nick, dataUser->realname, dataUser->password, 
                dataUser->host, dataUser->IP, dataUser->socket);
            switch (return_NewUser) {
                case IRC_OK:
                    #ifdef DEBUG_FORTE
                                IRCTADUser_ShowAll();
                    #endif
                         
                     
                    if ((IRCMsg_RplWelcome (&msgCommand, dataUser->prefix, dataUser->nick, dataUser->nick, dataUser->user, dataUser->host)) != IRC_OK){
                        freeDataUser(dataUser);
                        return NULL;
                    }
                    /*if(send(dataUser->socket, msgCommand, sizeof(msgCommand), 0) != 0){
                        printMsg("Welcome Enviado\n", LOG_DEBUG, 1);
                        printMsg(msgCommand, LOG_DEBUG, 1);
                    }*/
                    write(dataUser->socket , msgCommand , strlen(msgCommand));

                    free(msgCommand);
                    msgCommand = NULL;
                    return dataUser;
                    /*Mensaje welcome se envia fuera*/
                    break;
                case IRCERR_NICKUSED: 
                case IRCERR_INVALIDUSER:
                case IRCERR_INVALIDNICK:
                case IRCERR_INVALIDREALNAME:
                    IRCMsg_ErrNickNameInUse (&msgCommand, dataUser->prefix, dataUser->nick, nick);
                    break;
                case IRCERR_NOENOUGHMEMORY:
                case IRCERR_INVALIDHOST:
                case IRCERR_INVALIDIP:
                case IRCERR_INVALIDID:
                case IRCERR_INVALIDSOCKET:
                case IRCERR_NOMUTEX:
                    IRCMsg_Error (&msgCommand, dataUser->prefix, "Memory Error");
                    break;
            }

            if(msgCommand != NULL){
                write(dataUser->socket , msgCommand , strlen(msgCommand));
                free(msgCommand);
                msgCommand = NULL;
            }
            
            #ifdef DEBUG_FORTE
                        IRCTADUser_ShowAll();
            #endif
            break;
        }
    }
    return NULL;
}

int fun_default(char* str, DataUser *dataUser) {
    printMsg("Call a default function\n MSG: ", NOTICE, 0);
    printMsg(str, NOTICE, 0);
    SendUNKNOWNCOMMAND(str, dataUser);
    return 0;
}

int fun_topic(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char *channel = NULL;
    char *topic = NULL;
    long returnParse;
    char *MsgCommand = NULL;

    returnParse = IRCParse_Topic (str, &prefix, &channel, &topic);
    if(returnParse != IRC_OK){
        SendNEEDMOREPARAMS(str,dataUser);
        return -1;
    }
    free(prefix);
    prefix = NULL;

    /*Buscar channel*/
    char **listchannels;
    long numberOfChannels;

    if(IRCTADChan_GetList (&listchannels, &numberOfChannels, NULL) != IRC_OK){
        free(channel);
        channel = NULL;
        free(topic);
        topic = NULL;
        printMsg("Not enough memory to get list of channel\n", LOG_NOTICE, 1);
        return -1;
    }

    /*No se ha encontrado el canal, enviar mensaje de error*/
    if(getPosOfChanel(listchannels,numberOfChannels, channel) == -1){
        if(IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel) == IRC_OK){
            write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
        }
        return 1;
    }

    /*Liberar lista canales*/
    IRCTADChan_FreeList (listchannels, numberOfChannels);
    listchannels = NULL;

    if(topic == NULL){/*Enviar Topic al cliente*/
        if(IRCTAD_GetTopic (channel, &topic) == IRC_OK){
            if(topic != NULL){
                IRCMsg_RplTopic  (&MsgCommand, dataUser->prefix+1, dataUser->nick, channel, topic);
            }else{
                IRCMsg_RplNoTopic  (&MsgCommand, dataUser->prefix+1, dataUser->nick, channel);  
            }
        }
    }else{  /*Cambiar topic*/
        if(isProtectedTopic(channel)){
            if(isCreator(dataUser, channel)){
                if(IRCTAD_SetTopic (channel, dataUser->nick, topic) == IRC_OK){
                    IRCMsg_Topic (&MsgCommand, dataUser->prefix +1, channel, topic);
                    //IRCMsg_RplTopic  (&MsgCommand, dataUser->prefix +1, dataUser->nick, channel, topic);
                }else{
                    IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix+1, dataUser->nick, channel);
                }
            }else{
                IRCMsg_ErrChanOPrivsNeeded (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            }
        }else{
            if(IRCTAD_SetTopic (channel, dataUser->nick, topic) == IRC_OK){
                IRCMsg_Topic (&MsgCommand, dataUser->prefix +1, channel, topic);
                //IRCMsg_RplTopic  (&MsgCommand, dataUser->prefix +1, dataUser->nick, channel, topic);
            }else{
                IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix+1, dataUser->nick, channel);
            }
        }
    }

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }                                      

    free(channel);
    channel = NULL;
    free(topic);
    topic = NULL;
    return 0;
}

int fun_join(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char *channel = NULL;
    char *key = NULL;
    char *msg = NULL;
    char *MsgCommand = NULL;
    int flag_operator = 0;

    if(IRCParse_Join(str,&prefix,&channel,&key,&msg) != IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }
    free(prefix);
    prefix = NULL;

    /*Comprobar si el canal existe*/
    /*Buscar channel*/
    char **listchannels;
    long numberOfChannels;

    /*Error memoria*/
    if(IRCTADChan_GetList (&listchannels, &numberOfChannels, NULL) != IRC_OK){
        free(channel);
        channel = NULL;
        free(key);
        key = NULL;
        free(msg);
        msg = NULL;
        printMsg("Not enough memory to get list of channel\n", LOG_NOTICE, 1);
        return -1;
    }

    /*No se ha encontrado el canal, crear*/
    if(getPosOfChanel(listchannels,numberOfChannels, channel) == -1){
       if(IRCTADChan_New (channel, "", dataUser->nick, key, 0, NULL) != IRC_OK){
            free(channel);
            channel = NULL;
            free(key);
            key = NULL;
            free(msg);
            msg = NULL;
            /*No hay error de nombre se comprueba antes*/
            printMsg("Cannot create the channel\n", LOG_NOTICE, 1);
            return -2;
       }
       newChannels(listaGlobalChannles, channel);
       flag_operator = 1;
        #ifdef DEBUG
            //IRCTADChan_ShowAll (); /*Se queda colgado*/
        #endif

    } 

    /*Liberar lista canales*/
    IRCTADChan_FreeList (listchannels, numberOfChannels);
    listchannels = NULL;

    long ret;

    if(flag_operator){
        ret = IRCTAD_Join (channel, dataUser->nick, "o", key);
    }else{
        ret = IRCTAD_Join (channel, dataUser->nick, "", key);
    }

    /*Unirse*/
    switch (ret){
        case IRC_OK:
            IRCMsg_Join (&MsgCommand, dataUser->prefix +1, NULL, NULL, channel);
            if(flag_operator){
                newChannels(dataUser->listChannelsCreates, channel);  
            }
            break;
        case IRCERR_FAIL: /*Mal la contraseña*/
        case IRCERR_NOVALIDCHANNEL:
        case IRCERR_INVALIDCHANNELNAME:
            IRCMsg_ErrBadChannelKey (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            //IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        case IRCERR_USERSLIMITEXCEEDED:
            IRCMsg_ErrChannelIsFull (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        case IRCERR_BANEDUSERONCHANNEL:
            IRCMsg_ErrBannedFromChan (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        case IRCERR_NOINVITEDUSER:
            IRCMsg_ErrInviteOnlyChan (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        case IRCERR_YETINCHANNEL:
            IRCMsg_ErrUserOnChannel (&MsgCommand, dataUser->prefix, dataUser->nick, dataUser->nick, channel);
            break;
        default: /*IRCERR_NOVALIDUSER, IRCERR_INVALIDNICK, IRCERR_NOENOUGHMEMORY*/
            /*Errores no posible o del servidor*/
            free(channel);
            channel = NULL;
            free(key);
            key = NULL;
            free(msg);
            msg = NULL;
            return -3;
            break;
    }

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    } 

    free(MsgCommand);
    MsgCommand = NULL;

    free(msg);
    msg = NULL;
    free(channel);
    channel = NULL;
    free(key);
    key = NULL;
    return 0;
}

int fun_list(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char *channel = NULL;
    char *target = NULL;
    char *MsgCommand = NULL;

    if(IRCParse_List (str, &prefix, &channel, &target) != IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    free(prefix);
    prefix = NULL;

    /*Listar canales*/
    char **listchannels;
    long numberOfChannels;
    int i;

    if(IRCTADChan_GetList (&listchannels, &numberOfChannels, NULL) != IRC_OK){
        free(channel);
        channel = NULL;
        printMsg("Not enough memory to get list of channel\n", LOG_NOTICE, 1);
        return -1;
    } 

    char *topic = NULL;
    int numUser = 0; 
    char visible[25];
    if(channel == NULL){
        for(i=0; i< numberOfChannels; i++){
            bzero(visible, 25);
            if(isSecretChannel(listchannels[i])){
                continue;
            }
            IRCTAD_GetTopic (listchannels[i], &topic);
            numUser = IRCTADChan_GetNumberOfUsers (listchannels[i]);
            sprintf(visible,"%d",numUser);
            IRCMsg_RplList (&MsgCommand, dataUser->prefix, dataUser->nick,listchannels[i], visible, topic);
            if(MsgCommand != NULL){
                write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
            }   

            free(MsgCommand);
            MsgCommand = NULL;
            free(topic);
            topic = NULL;
            numUser = 0;
        } 
    }else{ /*Buscar unos canales en concreto*/
        char ** listChaneltTOK = NULL;
        long num_listChaneltTOK = 0;
        IRCParse_ParseLists (channel, &listChaneltTOK, &num_listChaneltTOK);
        for(i=0; i< num_listChaneltTOK; i++){
            bzero(visible, 25);
            if(isSecretChannel(channel)){
                continue;
            }
            /*No se ha encontrado el canal, enviar mensaje de error*/
            if(getPosOfChanel(listchannels,numberOfChannels, listChaneltTOK[i]) == -1){
                if(IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel) == IRC_OK){
                    write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
                }
            }

            IRCTAD_GetTopic (listChaneltTOK[i], &topic);
            numUser = IRCTADChan_GetNumberOfUsers (listChaneltTOK[i]);
            sprintf(visible,"%d",numUser);
            IRCMsg_RplList (&MsgCommand, dataUser->prefix, dataUser->nick,listChaneltTOK[i], visible, topic);
            if(MsgCommand != NULL){
                write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
            }   

            free(MsgCommand);
            MsgCommand = NULL;
            free(topic);
            topic = NULL;
            numUser = 0;
        } 
    }

    /*Liberar lista canales*/
    IRCTADChan_FreeList (listchannels, numberOfChannels);
    listchannels = NULL;
            
    /*Mensaje fin de list*/
    IRCMsg_RplListEnd (&MsgCommand, dataUser->prefix, dataUser->nick);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   

    free(MsgCommand);
    MsgCommand = NULL;
    free(channel);
    channel = NULL;
    return 0;
}

int fun_whois(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char *maskarray = NULL;
    char *target = NULL;
    char *MsgCommand = NULL;
    char serverhost[256];
    int i, j;

    if(IRCParse_Whois (str, &prefix, &target, &maskarray) != IRC_OK){
        IRCMsg_ErrNoNickNameGiven (&MsgCommand, dataUser->prefix +1, dataUser->nick);
        if(MsgCommand != NULL){
            write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
        }   
        free(MsgCommand);
        MsgCommand = NULL;
        return -1;
    }
    
    free(prefix);
    prefix = NULL;
    free(target);
    target = NULL;

    /*Informacion de todos los user*/
    long nelements = 0; 
    long *ids = NULL; 
    char **users = NULL; 
    char **nicks = NULL; 
    char **realnames = NULL; 
    char **passwords = NULL; 
    char **hosts = NULL; 
    char **IPs = NULL; 
    int *sockets = NULL; 
    long *modes = NULL; 
    long *creationTSs = NULL; 
    long *actionTSs = NULL;
    if(IRCTADUser_GetAllLists (&nelements, &ids, &users, &nicks, &realnames, &passwords, &hosts,
     &IPs, &sockets, &modes, &creationTSs, &actionTSs) !=IRC_OK){
        free(maskarray);
        maskarray = NULL;
        return -2;
    }

    for(i=0; i< nelements; i++){
        if(strcmp(nicks[i], maskarray) == 0){
            break;
        }
    }

    if(i == nelements){
        IRCTADUser_FreeAllLists(nelements,ids,users, nicks, realnames, passwords, hosts, IPs, sockets, modes, creationTSs, actionTSs);
        free(maskarray);
        maskarray = NULL;
        return -3;
    }
    /*Enviar Mensajes*/
    /*Info User*/
    IRCMsg_RplWhoIsUser (&MsgCommand, dataUser->prefix +1, nicks[i], nicks[i], users[i], hosts[i], realnames[i]);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   
    free(MsgCommand);
    MsgCommand = NULL;

    /*Info Server*/
    gethostname(serverhost, 255);
    IRCMsg_RplWhoIsServer (&MsgCommand, dataUser->prefix, nicks[i], nicks[i], serverhost, "Servidor Redes 2 IRC");
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   
    free(MsgCommand);
    MsgCommand = NULL;

    /*Info Operador*/
    /*IRCMsg_RplWhoIsOperator (&MsgCommand, dataUser->prefix, nicks[i], nicks[i]);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   
    free(MsgCommand);
    MsgCommand = NULL;*/

    /*Info canales*/
    char** arraychannellist = NULL;
    long nchannels = 0;
    char lista_channels[2000];

    if(IRCTAD_ListChannelsOfUserArray(users[i], nicks[i], &arraychannellist, &nchannels) == IRC_OK){
        bzero(lista_channels, 2000);
        for(j=0; j < nchannels; j++){
            if(IRCTAD_GetUserModeOnChannel (arraychannellist[j], nicks[i]) | IRCUMODE_OPERATOR){
                sprintf(lista_channels, "%s@%s ", lista_channels, arraychannellist[j]);
            }else{
                sprintf(lista_channels, "%s%s ", lista_channels, arraychannellist[j]);
            }
        }

        IRCMsg_RplWhoIsChannels(&MsgCommand, dataUser->prefix +1, nicks[i], nicks[i], lista_channels);
        if(MsgCommand != NULL){
            write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
        }   
        free(MsgCommand);
        MsgCommand = NULL;

        /*Liberar memoria*/
        for(j=0; j < nchannels; j++){
            free(arraychannellist[j]);
        }
        free(arraychannellist);
        arraychannellist = NULL;
    }

    /*Info IDLE*/
    IRCMsg_RplWhoIsIdle (&MsgCommand, dataUser->prefix, nicks[i], nicks[i], 0, NULL);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   
    free(MsgCommand);
    MsgCommand = NULL;

    /*Enviar Away si hay*/
    char * away = NULL;
    IRCTADUser_GetAway(0, users[i], nicks[i], realnames[i], &away);
    if(away != NULL){
        IRCMsg_RplAway (&MsgCommand, dataUser->prefix, nicks[i], nicks[i], away);
        if(MsgCommand != NULL){
            write(dataUser->socket , MsgCommand , strlen(MsgCommand));
        } 
        free(MsgCommand);
        MsgCommand = NULL;
        free(away);
        away = NULL;   
    }
    

    /*Mensaje fin de Whois*/
    IRCMsg_RplEndOfWhoIs (&MsgCommand, dataUser->prefix  +1, nicks[i], nicks[i]);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
    }   
    IRCTADUser_FreeAllLists(nelements,ids,users, nicks, realnames, passwords, hosts, IPs, sockets, modes, creationTSs, actionTSs);
    free(MsgCommand);
    MsgCommand = NULL;
    free(maskarray);
    maskarray = NULL;
    return 0;
}

int fun_nick(char* str, DataUser* dataUser) {
    char * prefix = NULL;
    char * nick = NULL;
    char * msg = NULL;
    char *MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Nick(str, &prefix, &nick, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    free(msg);
    msg = NULL;

    switch(IRCTADUser_Set(0, dataUser->user, dataUser->nick, dataUser->realname, dataUser->user, nick, dataUser->realname)){
        case IRC_OK:
            IRCMsg_Nick (&MsgCommand, dataUser->prefix +1, NULL, nick);
            if(MsgCommand != NULL){
                write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
            }   
            free(MsgCommand);
            MsgCommand = NULL;
            /*cambiamos el nick*/
            free(dataUser->nick);
            dataUser->nick = NULL;
            dataUser->nick = nick;

            /*Recalculamos el Prefix*/
            free(dataUser->prefix);
            dataUser->prefix = NULL;
            /*Crear Prefix*/
            IRC_Prefix (&(dataUser->prefix), dataUser->nick, dataUser->user, dataUser->host, "server.irc.redesii");
            break;
        case IRCERR_NICKUSED:
            IRCMsg_ErrNickNameInUse (&MsgCommand, dataUser->prefix, dataUser->nick, nick);
            if(MsgCommand != NULL){
                write(dataUser->socket , MsgCommand , strlen(MsgCommand)); 
            }   
            free(MsgCommand);
            MsgCommand = NULL;
            free(nick);
            nick = NULL;
            break;
        case IRCERR_INVALIDNICK:
        case IRCERR_INVALIDREALNAME:
        case IRCERR_INVALIDUSER:
        case IRCERR_NOENOUGHMEMORY:
        default: /*Errores no posibles o de memoria*/
            printMsg("Error to change name of memmory", LOG_NOTICE, 1);
        break;   
    }
    return 0;
}

int fun_names(char* str, DataUser* dataUser) {
    char * prefix = NULL;
    char * channel = NULL;
    char * target = NULL;
    char *MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Names (str, &prefix, &channel, &target)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    free(target);
    target = NULL;

    char *listnick = NULL;
    long numberOfUsers = 0;
    long ret;
    ret = IRCTAD_ListNicksOnChannel(channel, &listnick, &numberOfUsers);


    switch(ret){
        case IRC_OK:
            IRCMsg_RplNamReply (&MsgCommand, dataUser->prefix, dataUser->nick, "", channel, listnick);
            break;
        case IRCERR_NOVALIDCHANNEL:
            IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        default: /*Errores no posibles o de memoria*/
            printMsg("Error to change name of memmory", LOG_NOTICE, 1);
            free(listnick);
            listnick = NULL;
            free(channel);
            channel = NULL;
            return -1;
        break;   
    }

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    IRCMsg_RplEndOfNames (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    free(listnick);
    listnick = NULL;
    free(channel);
    channel = NULL;
    return 0;
}

int fun_privmsg(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * msgtarget = NULL;
    char * msg = NULL;
    char * MsgCommand = NULL;
    char * away = NULL;

    /*Sacar Datos*/
    if(IRCParse_Privmsg (str, &prefix, &msgtarget, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;

    if(msgtarget == NULL || strlen(msgtarget) < 1){
        free(msgtarget);
        msgtarget = NULL;
        free(msg);
        msg = NULL;
        return -2;
    }
    
    /*Lista de usuarios*/
    long nelements = 0; 
    long *ids = NULL; 
    char **users = NULL; 
    char **nicks = NULL; 
    char **realnames = NULL; 
    char **passwords = NULL; 
    char **hosts = NULL; 
    char **IPs = NULL; 
    int *sockets = NULL; 
    long *modes = NULL; 
    long *creationTSs = NULL; 
    long *actionTSs = NULL;
    if(IRCTADUser_GetAllLists (&nelements, &ids, &users, &nicks, &realnames, &passwords, &hosts,
     &IPs, &sockets, &modes, &creationTSs, &actionTSs) !=IRC_OK){
        free(msgtarget);
        msgtarget = NULL;
        return -3;
    }

    int pos = 0;
    int i;
    if(msgtarget[0] == '#'){ /*Enviar a un canal*/
        /*Sacar todos los usuarios de un canal*/
        char **arrayNickList;
        long numArrayNickList;
        if(IRCTAD_ListNicksOnChannelArray(msgtarget, &arrayNickList, &numArrayNickList) != IRC_OK){
            IRCTADUser_FreeAllLists(nelements,ids,users, nicks, realnames, passwords, hosts, IPs, sockets, modes, creationTSs, actionTSs);
            free(msg);
            msg = NULL;
            free(msgtarget);
            msgtarget = NULL;
            return -4;
        }
        for(i=0; i < numArrayNickList; i++){
            if(strcmp(arrayNickList[i], dataUser->nick) == 0){
                continue; /*Nos saltamos al que envia el mensaje*/
            }

            pos = getPosOfUser(nicks, nelements, arrayNickList[i]);
            if(pos != -1){
                IRCMsg_Privmsg (&MsgCommand, dataUser->prefix +1, msgtarget, msg);
                if(MsgCommand != NULL){
                    write(sockets[pos] , MsgCommand , strlen(MsgCommand));
                } 
                free(MsgCommand);
                MsgCommand = NULL;
            }else{
                /*This is not teorical posible nicks come from TAD*/
                printMsg("Mensaje to user doesnt exists\n", LOG_NOTICE, 0);
            }
        }

        for(i=0; i<numArrayNickList; i++){
            free(arrayNickList[i]);
        }
        free(arrayNickList);
        arrayNickList = NULL;

    }else{  /*Enviar a un usuario*/
        pos = getPosOfUser(nicks, nelements, msgtarget);
        if(pos != -1){
            IRCMsg_Privmsg (&MsgCommand, dataUser->prefix, nicks[pos], msg);
            if(MsgCommand != NULL){
                write(sockets[pos] , MsgCommand , strlen(MsgCommand));
            } 
            free(MsgCommand);
            MsgCommand = NULL;

            /*Enviar Away si hay*/
            IRCTADUser_GetAway(0, users[pos], nicks[pos], realnames[pos], &away);
            if(away != NULL){
                IRCMsg_RplAway (&MsgCommand, dataUser->prefix, nicks[pos], nicks[pos], away);
                //IRCMsg_Privmsg (&MsgCommand, dataUser->prefix, dataUser->nick, away);
                if(MsgCommand != NULL){
                    write(dataUser->socket , MsgCommand , strlen(MsgCommand));
                } 
                free(MsgCommand);
                MsgCommand = NULL;
                free(away);
                away = NULL;   
            }

        }else{
            IRCMsg_ErrNoSuchNick (&MsgCommand, dataUser->prefix, dataUser->nick, msgtarget);
            if(MsgCommand != NULL){
                write(dataUser->socket , MsgCommand , strlen(MsgCommand));
            } 
            free(MsgCommand);
            MsgCommand = NULL;
        }
    }

    /*Liberar todo*/
    IRCTADUser_FreeAllLists(nelements,ids,users, nicks, realnames, passwords, hosts, IPs, sockets, modes, creationTSs, actionTSs);
    free(msg);
    msg = NULL;
    free(msgtarget);
    msgtarget = NULL;
    return 0;
}

int fun_ping(char* str, DataUser* dataUser) {
    char * prefix = NULL;
    char * server = NULL;
    char * server2 = NULL;
    char * msg = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Ping (str, &prefix, &server, &server2, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    free(server2);
    server2 = NULL;
    free(msg);
    msg = NULL;

    if(IRCMsg_Pong (&MsgCommand, dataUser->prefix +1, dataUser->host, NULL, server)!= IRC_OK){
        return -2;
    }
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    /*Liberar datos*/
    free(server);
    server = NULL;
    return 0;
}

int fun_part(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * channel = NULL;
    char * msg = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Part (str, &prefix, &channel, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;


    long ret = 0;

    ret = IRCTAD_Part (channel, dataUser->nick);

    switch(ret){
        case IRC_OK: 
            IRCMsg_Part (&MsgCommand, dataUser->prefix, channel, msg);
            break;
        case IRCERR_NOVALIDCHANNEL: 
            IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
            break;
        default: /*No posibles o fallos de memoria*/
            /*IRCERR_UNDELETABLECHANNEL , IRCERR_NOVALIDUSER*/
            printMsg("Error to kick (Part) an user", LOG_NOTICE, 0);
            free(msg);
            msg = NULL;
            free(channel);
            channel = NULL;
            return -2;
            break;
    }

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    /*Liberar datos*/
    free(msg);
    msg = NULL;
    free(channel);
    channel = NULL;
    return 0;
}

int fun_kick(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * channel = NULL;
    char * user = NULL;
    char * comment = NULL;
    char * MsgCommand = NULL;
    long ret = IRCERR_NOVALIDUSER;

    /*Sacar Datos*/
    if(IRCParse_Kick (str, &prefix, &channel, &user, &comment)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }

    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    /*Lista de usuarios*/
    long nelements = 0; 
    long *ids = NULL; 
    char **users = NULL; 
    char **nicks = NULL; 
    char **realnames = NULL; 
    char **passwords = NULL; 
    char **hosts = NULL; 
    char **IPs = NULL; 
    int *sockets = NULL; 
    long *modes = NULL; 
    long *creationTSs = NULL; 
    long *actionTSs = NULL;
    if(IRCTADUser_GetAllLists (&nelements, &ids, &users, &nicks, &realnames, &passwords, &hosts,
     &IPs, &sockets, &modes, &creationTSs, &actionTSs) !=IRC_OK){
        free(comment);
        comment = NULL;
        free(channel);
        channel = NULL;
        free(user);
        user = NULL;
        return -2;
    }

    int pos = 0;
    pos = getPosOfUser(nicks, nelements, user);

    if(isCreator(dataUser, channel)){
        if(pos != -1){
                ret = IRCTAD_Part (channel, nicks[pos]);
            }

            switch(ret){
                case IRCERR_NOVALIDUSER: /*Pos = -1 -> Mensaje de err WasnoSuchNick*/
                case IRC_OK:
                    if(pos == -1){
                        IRCMsg_ErrWasNoSuchNick (&MsgCommand, dataUser->prefix, dataUser->nick, user);
                    }else{
                        IRCMsg_Kick (&MsgCommand, dataUser->prefix, channel, user, comment);
                    }
                    

                    break;
                case IRCERR_NOVALIDCHANNEL: 
                    IRCMsg_ErrNoSuchChannel (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
                    break;
                default: /*No posibles o fallos de memoria*/
                    /*IRCERR_UNDELETABLECHANNEL , IRCERR_NOVALIDUSER*/
                    printMsg("Error to kick (Kick) an user", LOG_NOTICE, 0);
                    free(comment);
                    comment = NULL;
                    free(channel);
                    channel = NULL;
                    free(user);
                    user = NULL;
                    return -3;
                    break;
            }
    }else{
        IRCMsg_ErrChanOPrivsNeeded (&MsgCommand, dataUser->prefix, dataUser->nick, channel);
    }

    if(MsgCommand != NULL){
        if(pos != -1){
            write(sockets[pos] , MsgCommand , strlen(MsgCommand));
        }
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 

    free(MsgCommand);
    MsgCommand = NULL;

    /*Liberar datos*/
    IRCTADUser_FreeAllLists(nelements,ids,users, nicks, realnames, passwords, hosts, IPs, sockets, modes, creationTSs, actionTSs);
    free(comment);
    comment = NULL;
    free(channel);
    channel = NULL;
    free(user);
    user = NULL;
    return 0;
}

int fun_away(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * msg = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Away (str, &prefix, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }
    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;


    if(IRCTADUser_SetAway (0, dataUser->user, dataUser->nick, dataUser->realname, msg) != IRC_OK){
        free(msg);
        msg = NULL;
        return -2;
    }


    if(msg == NULL){ /*Quitar Away*/
        IRCMsg_RplUnaway (&MsgCommand, dataUser->prefix, dataUser->nick);
    }else{ /*Poner Away*/
        //IRCMsg_Away (&MsgCommand, dataUser->prefix, msg);
        IRCMsg_RplNowAway (&MsgCommand, dataUser->prefix, dataUser->nick);
    }

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 

    free(MsgCommand);
    MsgCommand = NULL;

    /*Liberar memoria*/
    free(msg);
    msg = NULL;
    return 0;
}

int fun_quit(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * msg = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Quit (str, &prefix, &msg)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }
    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    IRCMsg_Quit (&MsgCommand, dataUser->prefix, msg);

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    free(msg);
    msg = NULL;
    return 0;
}

int fun_motd(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * target = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    if(IRCParse_Motd (str, &prefix, &target)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }
    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;
    free(target);
    target = NULL;

    /*Start of Motd*/
    IRCMsg_RplMotdStart (&MsgCommand, dataUser->prefix, dataUser->nick, "Server IRC - Redes II");
     if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    /*A short motd*/
    IRCMsg_RplMotd (&MsgCommand, dataUser->prefix, dataUser->nick, 
        "Welcome to this perfect server IRC by Miguel Angel y Dario");
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;
    
    /*End of Motd*/
    IRCMsg_RplEndOfMotd (&MsgCommand, dataUser->prefix, dataUser->nick);
    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    return 0;
}

int fun_mode(char* str, DataUser* dataUser){
    char * prefix = NULL;
    char * channeluser = NULL;
    char * mode = NULL;
    char * data = NULL;
    char * MsgCommand = NULL;

    /*Sacar Datos*/
    
    if(IRCParse_Mode (str, &prefix, &channeluser, &mode, &data)!= IRC_OK){
        SendNEEDMOREPARAMS(str, dataUser);
        return -1;
    }
    /*Liberar parametros que no nos interesan*/
    free(prefix);
    prefix = NULL;

    //MODE #channel \+k pass
    if(strcmp(mode, "\\+k") == 0){
        /*Data es la password*/
        IRCTADChan_SetPassword (channeluser, data);
        //IRCTADChan_ShowPasswords ();
    }else if(strcmp(mode, "+t") == 0){ //MODE #chanel +t
        /*Data es la password*/
        IRCTAD_Mode (channeluser, dataUser->nick, mode);
        changeProtectedTopic(channeluser);
        IRCMsg_Mode (&MsgCommand, dataUser->prefix, channeluser, data, mode);
    }else if(strcmp(mode, "+s") == 0){ //MODE #chanel +t
        /*Data es la password*/
        IRCTAD_Mode (channeluser, dataUser->nick, mode);
        changeSecretChannel(channeluser);
        IRCMsg_Mode (&MsgCommand, dataUser->prefix, channeluser, data, mode);
    }
    

    if(MsgCommand != NULL){
        write(dataUser->socket , MsgCommand , strlen(MsgCommand));
    } 
    free(MsgCommand);
    MsgCommand = NULL;

    return 0;
}
