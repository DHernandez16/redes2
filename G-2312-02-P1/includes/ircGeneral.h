/**
 * @file ircGeneral.h
 * @brief Funcion to manage the general configuration
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRC_GENERAL
#define IRC_GENERAL

#include "data_TAD.h"
#include "commands.h"

/**
 * @function daemonizar
 * @date 2017/2/9
 * @brief deamonizes caller process
 * @param int debug_log_mode: 
 * @param char * name: 
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int daemonizar(int debug_log_mode, char* name);

/**
 * @function socket_create
 * @date 2017/2/9
 * @brief Creates a socket and leaves it wating for connections
 * @param int type: specifies the communication semantics
 * @param int protocol: specifies a particular protocol to be used
 * @param int port: specifies the port number to read from
 * @return int: descriptor number to use the socket
 * @author: Dario Adrian Hernandez Barroso
 * @version: 1.2
 */
int socket_create(int type, int protocol, int port);

/**
 * @function thread_handler
 * @date 2017/2/12
 * @brief function that controls threads for each conection
 * @param int descriptor: descritpor number to comunicate with client 
 * @author: Darío Adrián Hernández Barroso
 * @return Null
 * @verison: 1.1
 */
void* thread_handler(void* desc);

/**
 * @function inicializatePFuncs
 * @date 2017/2/28
 * @brief function that inicializate the array of functions
 * @param int(* )(char *) defaultFunc: function default
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void inicializatePFuncs(int(* defaultFunc)(char *, DataUser *));

/**
 * @function registerFun
 * @date 2017/2/28
 * @brief register a function in the array of function
 * @param int(* )(char *) func: function to add
 * @param int pos: Position in the array
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void registerFun(int pos, int(* func)(char *, DataUser *));

#endif /*IRC_GENERAL*/