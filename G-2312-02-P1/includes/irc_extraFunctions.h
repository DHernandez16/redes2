/**
 * @file irc_extraFunctions.h 
 * @brief general extra Funcions to help
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRC_EXTRAFUNCTIONS_H
#define IRC_EXTRAFUNCTIONS_H

#include "irc.h"
#include "data_TAD.h"
#include "irc_extraFunctions.h"

/**
 * @function strclone
 * @date 2017/2/9
 * @brief Clona una cadena
 * @param str cadena a copiar 
 * @return string created
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
char * strclone(char * str);

/**
 * @function printMsg
 * @date 2017/2/9
 * @brief deamonizes caller process
 * @param char * msg: 
 * @param int log_mode: 
 * @param int perr: 
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void printMsg(char *msg, int log_mode, int perr);

/**
 * @function SendNEEDMOREPARAMS
 * @date 2017/2/28
 * @brief Send the error NEEDMOREPARAMS
 * @param char * str error comand
 * @param DataUser * dataUser Data of usser
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int SendNEEDMOREPARAMS(char * str, DataUser * dataUser);

/**
 * @function SendUNKNOWNCOMMAND
 * @date 2017/2/28
 * @brief Send the error UNKNOWNCOMMAND
 * @param char * str error comand
 * @param DataUser * dataUser Data of usser
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int SendUNKNOWNCOMMAND(char * str, DataUser * dataUser);

/**
 * @function getPosOfChanel
 * @date 2017/2/28
 * @brief Get position of channel in list
 * @param char **listchannels list of channels
 * @param long numberOfChannels dim of listchannel
 * @param char *channel name of chanel to search
 * @return position in list, -1 Error
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int getPosOfChanel(char **listchannels,long numberOfChannels, char * channel);

/**
 * @function getPosOfChanel
 * @date 2017/2/28
 * @brief Get position of channel in list
 * @param char **listusers list of user
 * @param long numberOfUsers dim of listusers
 * @param char *user name of user to search
 * @return position in list, -1 Error
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int getPosOfUser(char **listusers,long numberOfUsers, char * user);

#endif /*IRC_EXTRAFUNCTIONS_H*/
