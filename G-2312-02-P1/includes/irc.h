/**
 * @file irc.h 
 * @brief defines constants and includes 

 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRC
#define IRC
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <redes2/irc.h>

/*Sub libs*/
//#include "commands.h"
//#include "data_TAD.h"
//#include "irc_extraFunctions.h"
//#include "ircGeneral.h"

/*DIM CONSTANTS*/
#define BYTES_READ 512
#define DIM_NAME_SYSLOG 64
#define DIM_MSG  250

/*Configuration*/
#define SOCKET_PORT 6667
#define SECONDS_TO_PONG 30
#define DEBUG_LOG_MODE LOG_INFO

/*Debug*/
#define NOTHREAT
//#define DEBUG
//#define DEBUG_PRINT
//#define DEBUG_FORTE


#endif /* IRC */

