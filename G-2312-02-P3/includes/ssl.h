/**
 * @file ssl.h 
 * @brief defines constants and includes 

 * @author Miguel Angel Alvarez Rodriguez
 * @author 
 * @date 2017/5/2
 * @version 1.0
*/

#ifndef SSL_H
#define SSL_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <redes2/irc.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BYTEtoREAD 512


/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Esta función se encargará de realizar todas las llamadas necesarias para que la apli-
 * cación pueda usar la capa segura SSL.
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void inicializar_nivel_SSL();

/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief  Esta función se encargará de inicializar correctamente el contexto que será utilizado para
 * la creación de canales seguros mediante SSL. Deberá recibir información sobre las rutas a los certificados y
 * claves con los que vaya a trabajar la aplicación.
 * @param  char *CA Certificate of CA
 * @param  char *server_cliente_key Privated Key of Cliente or Server
 * @param  char* path of CA certificate
 * @return SSL_CTX * Contex SSL created an cofigureated
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
SSL_CTX * fijar_contexto_SSL(char *CA, char *server_cliente_key, char* path);

/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Dado un contexto SSL y un descriptor de socket esta función se encargará de
 * obtener un canal seguro SSL iniciando el proceso de handshake con el otro extremo.
 * @param  SSL_CTX * ctx Contex ssl
 * @param  SSL ** pSSL pointer *SSL to save SSl* created 
 * @param  int newSocket descriptor of socket to conect by ssl
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void conectar_canal_seguro_SSL(SSL_CTX * ctx, int newSocket, SSL ** pSSL);
 
/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Dado un contexto SSL y un descriptor de socket esta función se encargará de
 * bloquear la aplicación, que se quedará esperando hasta recibir un handshake por parte del cliente.
 * @param  SSL_CTX * ctx Contex ssl
 * @param  int newSocket descriptor of socket to acept by ssl
 * @param  SSL ** pSSL pointer *SSL to save SSl* created 
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void aceptar_canal_seguro_SSL(SSL_CTX * ctx, int newSocket, SSL ** pSSL);

/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Esta función comprobará una vez realizado el handshake que el canal de co-
 * municación se puede considerar seguro.
 * @param  SSL * ssl conexion ssl
 * @return 0 OK, <0 Error value
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int evaluar_post_connectar_SSL(SSL * ssl);
 
/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Esta función será el equivalente a la función de envío de mensajes que se realizó en la
 * práctica 1, pero será utilizada para enviar datos a través del canal seguro. Es importante que sea genérica y
 * pueda ser utilizada independientemente de los datos que se vayan a enviar.
 * @param  SSL * ssl conexion ssl
 * @param  char* data string to send
 * @return num of bytes send
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int enviar_datos_SSL(SSL * ssl, char* data);

/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Esta función será el equivalente a la función de lectura de mensajes que se realizó en la
 * práctica 1, pero será utilizada para enviar datos a través del canal seguro. Es importante que sea genérica y
 * pueda ser utilizada independientemente de los datos que se vayan a recibir.
 * @param  SSL * ssl conexion ssl
 * @param  char* data string to recive mensage
 * @return num of bytes recived
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int recibir_datos_SSL(SSL * ssl, char* data);
 
/**
 * @function inicializar_nivel_SSL
 * @date 2017/5/2
 * @brief Esta función liberará todos los recursos y cerrará el canal de comunicación seguro creado
 * previamente.
 * @param  SSL_CTX * ctx Contex ssl
 * @param  SSL * ssl conexion ssl
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void cerrar_canal_SSL(SSL_CTX * ctx, SSL * ssl);
 

#endif /* SSL_H */

