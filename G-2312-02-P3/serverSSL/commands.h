/**
 * @file commands.h 
 * @brief Functions to manage commands

 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef COMMANDS_H
#define COMMANDS_H

#include "irc.h"
#include "data_TAD.h"
#include "irc_extraFunctions.h"

/**
 * @function regUser
 * @date 2017/3/28
 * @brief Se encarga de realizar el registro de un usuario, recibe los mensajes, realiza el parseo
 * termina cuando el usuario se ha registrado correctament o devuleve un codigo de error   
 * @param int socket Socket asignado durante la conexion
 * @param  SSL * pSSL pointer ssl if conexion is done;
 * @return Infor User Registrado 
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
DataUser *regUser(int socket, SSL * pSSL);

/**
 * @function fun_default
 * @date 2017/2/21
 * @brief 
 * @param char* str 
 * @param DataUser* dataUser 
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_default(char* str, DataUser* dataUser);

/**
 * @function fun_topic
 * @date 04/12/2016
 * @brief  The TOPIC command is used to change or view the topic of a channel.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_topic(char* str, DataUser* dataUser);

/**
 * @function fun_join
 * @date 04/12/2016
 * @brief  The JOIN command is join to a channel.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Darío Adrián Hernández Barroso
 * @verison: 1.0
 */
int fun_join(char* str, DataUser* dataUser);

/**
 * @function fun_list
 * @date 04/12/2016
 * @brief  The list command is used to list channels and their topics.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_list(char* str, DataUser* dataUser);

/**
 * @function fun_list
 * @date 04/12/2016
 * @brief  This command is used to query information about particular user.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_whois(char* str, DataUser* dataUser);

/**
 * @function fun_nick
 * @date 2017/3/21
 * @brief 
 * @brief  This command is used to query information about particular user.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Darío Adrián Hernánde Barroso
 * @verison: 1.0
 */
int fun_nick(char* str, DataUser* dataUser);


/**
 * @function fun_names
 * @date 04/12/2016
 * @brief the NAMES string, a user can list all nicknames that are visible to him.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_names(char* str, DataUser* dataUser);

/**
 * @function fun_privmsg
 * @date 04/12/2016
 * @brief PRIVMSG is used to send private messages between users, as well as to send messages to channels.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_privmsg(char* str, DataUser* dataUser);

/**
 * @function fun_ping
 * @date 04/12/2016
 * @brief Implements protocol Ping-Pong
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_ping(char* str, DataUser* dataUser);

/**
 * @function fun_part
 * @date 04/12/2016
 * @brief The PART command causes the user sending the message to be removed
   from the list of active members
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_part(char* str, DataUser* dataUser);

/**
 * @function fun_kick
 * @date 04/12/2016
 * @brief The KICK command can be used to request the forced removal of a user from a channel.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_kick(char* str, DataUser* dataUser);

/**
 * @function fun_away
 * @date 04/12/2016
 * @brief With the AWAY command, clients can set an automatic reply string for any PRIVMSG commands directed at them.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_away(char* str, DataUser* dataUser);

/**
 * @function fun_quit
 * @date 04/12/2016
 * @brief A client session is terminated with a quit message.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_quit(char* str, DataUser* dataUser);

/**
 * @function fun_motd
 * @date 04/12/2016
 * @brief  The MOTD command is used to get the "Message Of The Day" of the given server.
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_motd(char* str, DataUser* dataUser);

/**
 * @function fun_mode
 * @date 04/12/2016
 * @brief  he MODE string is provided so that users may query and change the characteristics of a channel..
 * @param char* str Comand
 * @param DataUser * dataUser Data of user
 * @return O OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int fun_mode(char* str, DataUser* dataUser);

#endif /*COMMANDS_H*/