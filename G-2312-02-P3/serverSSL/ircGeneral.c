/**
 * @file ircGeneral.h
 * @brief Funcion to manage the general configuration
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */


#include "ircGeneral.h"

int (*pFuncs[IRC_MAX_COMMANDS])(char *, DataUser *);

int daemonizar(int debug_log_mode, char* name) {
    pid_t pid;
    int i, lfp;
    char buffer[DIM_NAME_SYSLOG];

    pid = fork();
    if (pid < 0) {
        perror("Cannot create a child\n");
        return -1;
    }; /*Fork Error*/
    if (pid > 0) exit(EXIT_SUCCESS); /*Father Exit*/

    umask(0); /*Chage mode mask*/
    setlogmask(LOG_UPTO(debug_log_mode)); /*Change mode logs shows*/

    if (setsid() < 0) { /*Obtein a new group proces*/
        syslog(LOG_ERR, "Error creating a new SID for the child process.");
        return -2;
    }

    if ((chdir("/tmp")) < 0) { /*Change to \tmp*/
        syslog(LOG_ERR, "Error changing the current working directory = \"/tmp\"");
        return -3;
    }

    strncpy(buffer, name, DIM_NAME_SYSLOG);
    buffer[DIM_NAME_SYSLOG - 1] = '\0'; /*Avoid names longer*/
    openlog(buffer, LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL2); /*Open Log*/

    /* Close all descriptors */
    for (i = getdtablesize(); i >= 0; --i) {
        close(i);
    }

    /* Only one proces */
    buffer[DIM_NAME_SYSLOG - 6] = '\0'; /*.lock*/
    sprintf(buffer, "%s%s", buffer, ".lock");

    /* Only first instance continues */
    lfp = open(buffer, O_RDWR | O_CREAT, 0640); /* Cannot open lockfile*/
    if (lfp < 0) {
        syslog(LOG_ERR, "Cannot open lockfile");
        closelog();
        return -4;
    }

    if (lockf(lfp, F_TLOCK, 0) < 0) {/* Cannot lock the file*/
        syslog(LOG_ERR, "Cannot lock the file");
        closelog();
        return -5;
    }

    /* Record pid to lockfile */
    sprintf(buffer, "%d\n", getpid());
    write(lfp, buffer, strlen(buffer));

    syslog(LOG_INFO, "****Done daemon process****");

    if ((chdir("/")) < 0) { /*Change to \root directory*/
        syslog(LOG_ERR, "Error changing the current working directory = \"/\"");
        closelog();
        return -6;
    }

    return 0;
}

int socket_create(int type, int protocol, int port) {
    struct sockaddr_in servaddr;
    int descriptor;

    if ((descriptor = socket(type, protocol, 0)) == -1) {
        /*Cuando ejecutes con daemon no tienes printf, usa syslog*/
        printMsg("Error en socket\n", ERROR, 1);
        return -1;
    }

    bzero(&servaddr, sizeof (servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(port);

    if (bind(descriptor, (struct sockaddr*) &servaddr, sizeof (servaddr)) == -1) {
        printMsg("Error en bind\n", ERROR, 1);
        return -1;
    }

    if (listen(descriptor, 10) == -1) {
        printMsg("Error en listen\n", ERROR, 1);
        return -1;
    }

    return descriptor;
}

void* thread_handler(void* desc) {
    /*Variables de lectura*/
    int bytesread; /*Bytes leidos*/
    char str_read[BYTES_READ]; /*Cadena para leer mensaje*/
    char* string_command; /*cadena con el comando a analizar*/
    char *str_aux = NULL; /*Cadena para partir comandos aun no analizados*/
    long comm_ret; /*Tipo de comando a parsear*/
    int socket = -1;
    SSL* pSSL = NULL;
    SSL_CTX * ctx = NULL;

    DataUser *dataUser = NULL;

    /*CODGIO*/
    printMsg("Hilo creado\n", LOG_DEBUG, 0);

    if(ssl){
        /*Inicializar SSL*/
        inicializar_nivel_SSL();
        /*Crear contexto*/
        ctx = fijar_contexto_SSL("./certs/ca.pem", "./certs/servidor.pem", "certs");

        printMsg("----> Contexto Fijado\n", LOG_NOTICE, 0);
        /*Aceptar canal seguro*/
        aceptar_canal_seguro_SSL(ctx, socket, &pSSL);

        if(evaluar_post_connectar_SSL(pSSL) != 0){
            fprintf(stderr, "Cannot verify certificate\n");
            return NULL;
        }
    }

    /*Datos del usuario que se conecta*/
    /*dataThreat = (DataThreat *) desc;
    socket = dataThreat->comm_fd;
    IP = dataThreat->sa_data;
    */
    socket = ((int *) desc)[0];
    /*printf("1 IP -> %s\n", dataThreat->sa_data);
    printf("1 socket -> %d\n", dataThreat->comm_fd);
    */
    /*registramos la señal del pong, TODO: Ver como pasarle el descriptor*/
    //signal(SIGALRM, pong_protocol);
    /*ponemos la alarma del pong*/
    //alarm(SECONDS_TO_PONG);

    dataUser = regUser(socket, pSSL);

    /*Saltar resto si registro no es*/
    /*flag_register = -1 -> El cliente se ha desconectado*/
    if (dataUser !=  NULL) {    /*Resto de comandos*/

        /*Leer resto de comandos*/
        while (1) {
            /*Leer un comando*/
            bzero(str_read, BYTES_READ);
            if(ssl){
                bytesread = recibir_datos_SSL(dataUser->ssl, str_read);
            }else{
                bytesread = read(socket, str_read, BYTES_READ);
            }
            
            if (bytesread == 0) {
                break;
            }
            str_aux = str_read; /*Valor inicial de str_aux mensaje recibido*/
            do {/*Sacar comandos de un mensaje*/
                /*str_aux resto de mensajes sin analizar*/
                str_aux = IRC_UnPipelineCommands(str_aux, &string_command); /*string_command = COMMAND string, unpiped_string = resto de cadena*/

                /*Get ID del comando*/
                comm_ret = IRC_CommandQuery(string_command);

                if (comm_ret < 0 || comm_ret > IRC_MAX_COMMANDS) {
                    SendUNKNOWNCOMMAND(string_command, dataUser);
                    printMsg("Error comando no valido\n", LOG_NOTICE, 0);
                } else {
                    pFuncs[comm_ret](string_command, dataUser);
                }
                free(string_command);
                string_command = NULL;

                if(comm_ret == QUIT){
                    break;
                }

            } while (str_aux != NULL);
            if(comm_ret == QUIT){
                printMsg("Quit Recived\n", LOG_INFO, 0);
                break;
            }
        }
        IRCTAD_Quit (dataUser->nick);
        IRCTADUser_Delete (0, dataUser->user, dataUser->nick, dataUser->realname);
    }

    if(ssl){
        cerrar_canal_SSL(ctx, pSSL);
    }

    close(dataUser->socket);

    freeDataUser(dataUser);
    dataUser = NULL;
    printMsg("Finalizando ejecucion del hilo\n", LOG_INFO, 0);

    #ifndef NOTHREAT
        pthread_detach(pthread_self());
    #endif

    return NULL;
}

void inicializatePFuncs(int(* defaultFunc)(char *, DataUser *)) {
    int i;
    if (defaultFunc == NULL) return;

    for (i = 0; i < IRC_MAX_COMMANDS; i++) {
        pFuncs[i] = defaultFunc;
    }

    registerFun(TOPIC, fun_topic);
    registerFun(JOIN, fun_join);
    registerFun(LIST, fun_list);
    registerFun(WHOIS, fun_whois);
    registerFun(NICK, fun_nick);
    registerFun(NAMES, fun_names);
    registerFun(PRIVMSG, fun_privmsg);
    registerFun(PING, fun_ping);
    registerFun(PART, fun_part);
    registerFun(KICK, fun_kick);
    registerFun(AWAY, fun_away);
    registerFun(QUIT, fun_quit);
    registerFun(MOTD, fun_motd);
    registerFun(MODE, fun_mode);
}

void registerFun(int pos, int(* func)(char *, DataUser *)) {
    if (pos >= 0 && pos < IRC_MAX_COMMANDS && func != NULL) {
        pFuncs[pos] = func;
    }
}
