/**
 * @file Data_TAD.c
 * @brief Funcion to handel TAD DataUser y List channels
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "data_TAD.h"
#include "irc_extraFunctions.h"


void freeDataUser(DataUser * dataUser){
    if(dataUser != NULL){
        free(dataUser->user);
        free(dataUser->nick);
        free(dataUser->realname);
        free(dataUser->password);
        free(dataUser->host);
        free(dataUser->IP);
        free(dataUser->prefix);
        freeListChannels(dataUser->listChannelsCreates);
        free(dataUser);
    }
}

int createListGlobalChannels(){
     if((listaGlobalChannles  = (NodeListChannels *) calloc (sizeof (NodeListChannels), 1)) != NULL){
        listaGlobalChannles->channel = strclone("Start Channles");
        listaGlobalChannles->next = NULL;
        listaGlobalChannles->flagProtectedTopic = 0;
        return 0;
     }
     return -1;
}

void newChannels(NodeListChannels *list, char * channel){
    NodeListChannels *auxlist;
    auxlist = (NodeListChannels *) calloc (sizeof (NodeListChannels), 1);

    if(auxlist == NULL){
        return;
    }

    auxlist->channel = strclone(channel);
    auxlist->next = NULL;
    auxlist->flagProtectedTopic = 0;
    auxlist->flagSecretChannel = 0;

    while(list->next != NULL){
        list = list->next;
    }

    list->next = auxlist;
}

int isCreator(DataUser *dataUser, char *channel){
    NodeListChannels *list = dataUser->listChannelsCreates;
    do{
        if(strcmp(list->channel, channel) == 0){
            return 1;
        }
        list = list->next;
        
    } while(list != NULL);

    return 0;
}

void freeListChannels(NodeListChannels *list){
    NodeListChannels *auxNext;

    auxNext = list;
    while(auxNext != NULL){
        auxNext = list->next;
        free(list->channel);
        free(list);
        list = auxNext;
    }
}

void freeALLChanels(){
    int i;
    char **listchannels;
    long numberOfChannels;

    if(IRCTADChan_GetList (&listchannels, &numberOfChannels, NULL) != IRC_OK){
        printMsg("Not enough memory to get list of channel\n", LOG_NOTICE, 1);
        return;
    } 

    for(i=0; i< numberOfChannels ; i++){
        IRCTADChan_Delete (listchannels[i]);
        //printf("Eilimando chat: -> %s\n",listchannels[i]);
    }

    /*Liberar lista canales*/
    IRCTADChan_FreeList (listchannels, numberOfChannels);
    listchannels = NULL;
    numberOfChannels = 0;
}

void changeProtectedTopic(char *channel){
    NodeListChannels *list = listaGlobalChannles;
    do{
        if(strcmp(list->channel, channel) == 0){
            list->flagProtectedTopic = !list->flagProtectedTopic;
        }
        list = list->next;


    } while(list != NULL);
}

int isProtectedTopic(char *channel){
    NodeListChannels *list = listaGlobalChannles;
    do{
        if(strcmp(list->channel, channel) == 0){
            return list->flagProtectedTopic;
        }
        list = list->next;


    } while(list != NULL);

    return 0;
}

void changeSecretChannel(char *channel){
    NodeListChannels *list = listaGlobalChannles;
    do{
        if(strcmp(list->channel, channel) == 0){
            list->flagSecretChannel = !list->flagSecretChannel;
        }
        list = list->next;
    } while(list != NULL);
}

int isSecretChannel(char *channel){
    NodeListChannels *list = listaGlobalChannles;
    do{
        if(strcmp(list->channel, channel) == 0){
            return list->flagSecretChannel;
        }
        list = list->next;


    } while(list != NULL);

    return 0;
}
