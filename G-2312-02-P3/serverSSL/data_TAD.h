/**
 * @file data_TAD.h
 * @brief Funcion to handel TAD DataUser y List channels
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef DATA_TAD_H
#define DATA_TAD_H

#include "irc.h"

typedef struct _NodeListChannels {
	char * channel;
	struct _NodeListChannels * next;
	int flagProtectedTopic;
	int flagSecretChannel;
} NodeListChannels;


/* Array de Canales*/
NodeListChannels *listaGlobalChannles;

typedef struct _DataUser {
	char *user;
	char *nick;
	char *realname;
	char *password;
	char *host;
	char *IP;
	char *prefix;
	NodeListChannels *listChannelsCreates;
	int socket;
	SSL* ssl;
} DataUser;

/**
 * @function freeDataUser
 * @date 2017/3/28
 * @brief Libera la estructura DataUser 
 * @param dataUser Usuario a liberar
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void freeDataUser(DataUser * dataUser);

/**
 * @function createListGlobalChannels
 * @date 04/12/2016
 * @brief  Inicializa el array de canales
 * @return 1 OK, <0 Error Code
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
int createListGlobalChannels();

/**
 * @function newChannels
 * @date 2017/2/9
 * @brief Agrega un canal a la lista de canales creados
 * @param NodeListChannels *list lista de canales
 * @param char * channel Canal a añadir
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void newChannels(NodeListChannels *list, char * channel);

/**
 * @function isCreator
 * @date 2017/2/9
 * @brief Comprueba si un usuario es el creador
 * @param DataUser * dataUser Data of user
 * @param char * channel Canal a comprobar
 * @return 1 Si es el creador, 0 si no
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int isCreator(DataUser *dataUser, char *channel);

/**
 * @function freeListChannels
 * @date 2017/3/28
 * @brief Libera la estructura NodeListChannels 
 * @param NodeListChannels *list lista a liberar
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void freeListChannels(NodeListChannels *list);

/**
 * @function freeDataUser
 * @date 2017/3/28
 * @brief Free all chanels
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.0
 */
void freeALLChanels();

/**
 * @function changeProtectedTopic
 * @date 2017/2/9
 * @brief Indica aun canal que habilite el modo Protected
 * @param char * channel Canal a comprobar
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void changeProtectedTopic(char *channel);

/**
 * @function setProtectedTopic
 * @date 2017/2/9
 * @brief Indica si un canal tiene habilitado el modo Protected
 * @param char * channel Canal a comprobar
 * @return 1 Si esta hablitado, 0 si no
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int isProtectedTopic(char *channel);

/**
 * @function changeProtectedTopic
 * @date 2017/2/9
 * @brief Indica aun canal que habilite el modo Protected
 * @param char * channel Canal a comprobar
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void changeSecretChannel(char *channel);

/**
 * @function setProtectedTopic
 * @date 2017/2/9
 * @brief Indica si un canal tiene habilitado el modo Protected
 * @param char * channel Canal a comprobar
 * @return 1 Si esta hablitado, 0 si no
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
int isSecretChannel(char *channel);

#endif /*DATA_TAD_H*/

