#include "irc.h"
#include "data_TAD.h"
#include "ircGeneral.h"
#include "irc_extraFunctions.h"

/*Variable global para el socket del servidor*/
int socket_id;

/*Variables globales gestion de hilos*/

void SIGINT_handler();
int getFreePosition(pthread_t **array_hilos, int *maxDim);

int main(int argc, char** argv) {
    int comm_fd;
    struct sockaddr addr;
    int size;

    ssl = 0;

    if(argc >= 2 && (strcmp(argv[1], "-ssl") == 0)){
        ssl = 1;
    }

    #ifdef NOTHREAT
        pthread_t hilo_id;
    #endif
    
    signal(SIGINT,SIGINT_handler);

    inicializatePFuncs(fun_default);

    #ifdef DEBUG_PRINT
        printf("****Modo DEBUG*****\n\n");
    #else
        daemonizar(DEBUG_LOG_MODE, "ServerIRC");
    #endif

    if(ssl){
        printMsg("SSL conexion\n", LOG_NOTICE, 0);
        socket_id = socket_create(AF_INET,SOCK_STREAM,6669);
    }else{
        socket_id = socket_create(AF_INET,SOCK_STREAM,SOCKET_PORT);
    }
    if(socket_id==-1){
        printMsg("Error to create socket \n", LOG_ERR, 1);
        return (EXIT_FAILURE);
    }

    if(createListGlobalChannels() != 0){
        printMsg("Error to create List Channels \n", LOG_ERR, 1);
        return (EXIT_FAILURE);
    }

    /*Loop that waits for connections and creates threads */
    while(1){
        /*Limpiamos los datos a enviar*/
        bzero(&addr, sizeof(struct sockaddr));

        /*Aceptamos la conexion*/
        //if((comm_fd = accept(socket_id, &addr, NULL)) == -1){
        size = sizeof(addr);
        if((comm_fd = accept(socket_id, &addr, (socklen_t *) &size)) == -1){
        //if((comm_fd = accept(socket_id, (struct sockaddr*) NULL, NULL)) == -1){
            printMsg("Connection refused\n", LOG_ERR, 1);
            continue;
        }else{
            printMsg("Connection recived\n", NOTICE, 0);
        }
        
        /*Lanzar un hilo*/      
        #ifdef NOTHREAT
            /*Mandar a un hilo la llamada*/
            if (pthread_create(&hilo_id, NULL, &thread_handler, (void *)&comm_fd) != 0) {
                printMsg("Error to create a threat\n", LOG_ERR, 0);
            }
        #else
            /*Analizar como llamada a funcion*/
            thread_handler((void *) &dataThreat);
        #endif
    }
    return (EXIT_SUCCESS); 
}

void SIGINT_handler(){
    /*liberar el puerto*/
    close(socket_id);
    freeALLChanels();
    freeListChannels(listaGlobalChannles);
    printMsg("SIGINT - Fin de programa\n", NOTICE, 0);
    #ifndef DEBUG_PRINT
        /*Close Log*/
        closelog();
    #endif
    exit(EXIT_SUCCESS);
}
