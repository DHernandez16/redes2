# include "ssl.h"

void inicializar_nivel_SSL(){
	SSL_load_error_strings();
	SSL_library_init();	
    //OpenSSL_add_ssl_algorithms();
}

SSL_CTX * fijar_contexto_SSL(char *CAfile, char *server_cliente_key, char* CApath){
	const SSL_METHOD *method;
    SSL_CTX *ctx;

    /*Create Method*/
    method = SSLv23_method();

    /*Create Contex*/
    ctx = SSL_CTX_new(method);
    if (!ctx) {
		perror("Unable to create SSL context");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
    }

    /*Validar certificados recibidos por la aplicacion*/
    if (SSL_CTX_load_verify_locations(ctx,CAfile,CApath) == 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
    /*Utilizar certificados CA*/
    if(SSL_CTX_set_default_verify_paths(ctx) == 0){
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    /* Set the key and cert */
    if (SSL_CTX_use_certificate_chain_file(ctx, server_cliente_key) <= 0) {
        ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, CAfile, SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
    }
    SSL_CTX_set_verify (ctx, SSL_VERIFY_NONE , NULL);

    return ctx;
}


void conectar_canal_seguro_SSL(SSL_CTX * ctx, int newSocket, SSL ** pSSL){
    if( pSSL == NULL){
        return;
    }
    pSSL[0] = SSL_new(ctx);
    SSL_set_fd(pSSL[0], newSocket);
    if(SSL_accept(pSSL[0]) != 1){
        perror("Cannot connect");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

}

void aceptar_canal_seguro_SSL(SSL_CTX * ctx, int newSocket, SSL ** pSSL){
    if( pSSL == NULL){
        return;
    }
	pSSL[0] = SSL_new(ctx);
    SSL_set_fd(pSSL[0], newSocket);
	if(SSL_connect(pSSL[0]) != 1){
        perror("Cannot accept");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

}

int evaluar_post_connectar_SSL(SSL * ssl){
    X509 * certificate = NULL;
    certificate = SSL_get_peer_certificate(ssl);

    if(certificate == NULL){
        return -1;
    }else{
        X509_free(certificate);
    }

    if(SSL_get_verify_result(ssl) != X509_V_OK){
        return -2;
    }

    return 0;
}
 
int enviar_datos_SSL(SSL * ssl, char* data){
	if(ssl != NULL && data != NULL){
		return SSL_write(ssl, data, strlen(data));
	}	
    return 0;
}

int recibir_datos_SSL(SSL * ssl, char* data){
	if(ssl != NULL && data != NULL){
		return SSL_read(ssl, data, BYTEtoREAD);
	}
    return 0;
}

void cerrar_canal_SSL(SSL_CTX * ctx, SSL * ssl){
    if(ctx != NULL){
        SSL_CTX_free(ctx);
    }
    //EVP_cleanup();
    if(ssl != NULL){
        SSL_shutdown(ssl);
        SSL_free(ssl);
    }
}

