/**
 * @file irc_echo_types.h 
 * @brief defines constants and includes 

 * @author Miguel Angel Alvarez Rodriguez
 * @author 
 * @date 2017/5/2
 * @version 1.0
*/

#ifndef IRC_ECHO_TYPES_H
#define IRC_ECHO_TYPES_H

#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <redes2/irc.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdlib.h>


#define DIM_MSG  250
#define SOCKET_PORT 6667
#define NOTHREAT

#endif /* IRC_ECHO_TYPES_H */

