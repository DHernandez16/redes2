#include "irc_echo_types.h"
#include "ssl.h"
/*Variable global para el socket del servidor*/
int socket_id;

void SIGINT_echo_handler();
void* echo_handler(void* desc);

int main(int argc, char** argv) {
    int newSocket;
    struct sockaddr addr;
    struct sockaddr_in servaddr;
    int size;
    int socket_id;
    SSL_CTX * ctx = NULL;
    SSL * ssl = NULL;
    int bytesread; /*Bytes leidos*/
    char str_read[BYTEtoREAD]; /*Cadena para leer mensaje*/
    
    /*Signal Manejador*/
    signal(SIGINT,SIGINT_echo_handler);   

    /*Create Socket*/
    if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        /*Cuando ejecutes con daemon no tienes printf, usa syslog*/
        fprintf(stderr, "Error en socket\n");
        return -1;
    }

    /*Prepare socket*/
    bzero(&servaddr, sizeof (servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(SOCKET_PORT);
    if (bind(socket_id, (struct sockaddr*) &servaddr, sizeof (servaddr)) == -1) {
        fprintf(stderr, "Error en bind\n");
        return -1;
    }

    /*Start socket*/
    if (listen(socket_id, 10) == -1) {
        fprintf(stderr, "Error en listen\n");
        return -1;
    }

    /*Limpiamos los datos a enviar*/
    bzero(&addr, sizeof(struct sockaddr));

    /*Aceptamos la conexion*/
    size = sizeof(addr);
    fprintf(stderr, "Waiting for conexion...\n");

    if((newSocket = accept(socket_id, &addr, (socklen_t *) &size)) == -1){
        fprintf(stderr, "Connection refused\n");
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "Connection recived\n");

    /*Inicializar SSL*/
    inicializar_nivel_SSL();
    /*Crear contexto*/
    ctx = fijar_contexto_SSL("./certs/ca.pem", "./certs/servidor.pem", "certs");

    /*Aceptar canal seguro*/
    aceptar_canal_seguro_SSL(ctx, newSocket, &ssl);

    if(evaluar_post_connectar_SSL(ssl) != 0){
        fprintf(stderr, "Cannot verify certificate\n");
    }

    while (1) {
        /*Leer un comando*/
        bzero(str_read, BYTEtoREAD);

        bytesread = recibir_datos_SSL(ssl, str_read);
        if (bytesread == 0) {
            break;
        }
        
        enviar_datos_SSL(ssl, str_read);

        if(strncmp(str_read, "exit", 4) == 0){
            break;
        }
    }

    cerrar_canal_SSL(ctx, ssl);
    close(newSocket);
    close(socket_id);

    return (EXIT_SUCCESS); 
}

void SIGINT_echo_handler(){
    /*liberar el puerto*/
    close(socket_id);
    fprintf(stderr, "SIGINT recived\n\n");
    exit(EXIT_SUCCESS);
}




