#include "irc_echo_types.h"
#include "ssl.h"

/*Variable global para el socket del servidor*/
int sockfd;

void SIGINT_echo_handler();

int main(int argc, char **argv) {
    int sockfd, portno, n;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char hostname_defualt[25];
    char buf[BYTEtoREAD];
    SSL_CTX * ctx = NULL;
    SSL * ssl = NULL;

    /* check command line arguments */
    if (argc != 3) {
		bzero(hostname_defualt, 25);
		strcpy(hostname_defualt, "localhost");
		hostname = hostname_defualt;
		portno = 6667;
    }else{
    	hostname = argv[1];
    	portno = atoi(argv[2]);
    }
    

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        fprintf(stderr, "ERROR opening socket\n");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0){
      fprintf(stderr, "ERROR connecting\n");
      perror("\n");
      return EXIT_FAILURE;
    }


    /*Inicializar SSL*/
    inicializar_nivel_SSL();
    /*Crear contexto*/
    ctx = fijar_contexto_SSL("./certs/ca.pem", "./certs/cliente.pem", "certs");

    /*conectar canal seguro*/
    conectar_canal_seguro_SSL(ctx, sockfd, &ssl);

  	while(1){
	    /* get message line from the user */
	    bzero(buf, BYTEtoREAD);
	    fgets(buf, BYTEtoREAD, stdin);

	    /* send the message line to the server */
        n = enviar_datos_SSL(ssl, buf);
	    if (n < 0) 
	      fprintf(stderr, "ERROR writing to socket");

	    /* print the server's reply */
	    bzero(buf, BYTEtoREAD);
        n = recibir_datos_SSL(ssl, buf);
	    if (n < 0) 
	      fprintf(stderr, "ERROR reading from socket");
	    printf("%s", buf);
        if(strncmp(buf, "exit", 4) == 0){
            break;
        }
	}
    return 0;
}

void SIGINT_echo_handler(){
    /*liberar el puerto*/
    close(sockfd);
    fprintf(stderr, "SIGINT recived\n\n");
    exit(EXIT_SUCCESS);
}

