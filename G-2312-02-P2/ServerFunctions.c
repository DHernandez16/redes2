/**
 * @file ServerFunctions.c
 * @brief Handel commands of server
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "ServerFunctions.h"
#include "ircGeneral.h"
#include "irc_auxFunctions.h"
//#include "irc_client.h"

void addPServerFunToArray(int pos, int (*fun)(char*)){
	if(pos >= 0 && pos < DIM_SERVERUFUNS){
		serverpfuns[pos] = fun;
	}
}

void inicializatePServerFuns(int (*def_fun)(char*)){
	int i; 
	for(i=0; i< DIM_SERVERUFUNS; i++){
		serverpfuns[i] = def_fun;
	}

	addPServerFunToArray(JOIN, ServerJoin);
	//addPServerFunToArray(UHELP, FunUhelp);
	//addPServerFunToArray(ULIST, FunUlist);
	//addPServerFunToArray(UJOIN, FunUjoin); /*OK*/
	//addPServerFunToArray(UPART, FunUpart);
	//addPServerFunToArray(UQUIT, FunUquit);
	//addPServerFunToArray(UNICK, FunUnick);
	//addPServerFunToArray(UAWAY, FunUaway);
	//addPServerFunToArray(UWHOIS, FunUwhois);
}

int FunServerDefault (char* str){
	printMsg("ServerCommand not handel", LOG_NOTICE, 0);
	printMsg(str, LOG_NOTICE, 0);
	return 0;
}


/*Funciones de la llamada listen, que escucha del servidor*/

int ServerJoin (char* str){
	/*TODO */
	//IRCInterface_AddNewChannel(channel, 0);

	//IRCInterface_AddNickChannel(cln->nick);
	return -1;
}


