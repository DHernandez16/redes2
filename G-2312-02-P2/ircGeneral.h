/**
 * @file ircGeneral.h 
 * @brief defines generalfuntios
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRCGENRAL
#define IRCGENRAL

#include "irc_client.h"
/*Estructura para guardar variables del cliente*/
typedef struct{
	int server_socket; /*Socket de comunicación con el servidor*/
	char* nick ; /*nick del cliente*/
	char* prefix; /*Prefix del cliente*/
}CLIENTE;

/*Variables globales para la práctica*/
CLIENTE cln; /*Declaramos un cliente global, que se usará en cada conexión*/


/**
 * @function socket_create
 * @date 2017/2/9
 * @brief Creates a socket and leaves it wating for connections
 * @param int type: specifies the communication semantics
 * @param int protocol: specifies a particular protocol to be used
 * @param int port: specifies the port number to read from
 * @return int: descriptor number to use the socket
 * @author: Dario Adrian Hernandez Barroso
 * @version: 1.2
 */
int socket_create(int type, int protocol, int port);


ssize_t SendToServer (char* msg);
#endif /* IRCGENRAL */

