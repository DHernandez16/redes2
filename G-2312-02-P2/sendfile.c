/**
 * @file sendfile.c
 * @brief Functions to manage protocol to sendfiles
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/5
 * @version 1.0
 */
#include "sendfile.h"
#include "ircGeneral.h"


int IRCParse_faccept(char ** ip, int * port){
	char command_aux[DIM_FSEND];
	char accept[DIM_FSEND];
	if(ip == NULL || port == NULL){
		return -1;
	}

	sscanf(command_aux, "%s %s %d", accept, ip[0], port);
	return 0;
}

int IRCMSG_faccept(char ** msg, char *nickDest, char* ip, int port, char* prefix){
	char command_accept_aux[DIM_FSEND];
	if(msg == NULL){
		return -1;
	}
	if(nickDest == NULL || ip == NULL || port <= 0){
		return -2;
	}

	/*Limpiar cadena*/
	bzero(command_accept_aux, DIM_FSEND);

	/*Crear /faccepet <ip> <port>*/
	sprintf(command_accept_aux, "/faccept %s %d", ip, port);

	/*Enmascarar en un PrivMSG*/
	if(IRCMsg_Privmsg (msg, prefix, nickDest, command_accept_aux) != IRC_OK){
		return -3;
	}

	return 0;
}

int IRCMSG_fsend(char ** msg, char *nickDest, char* namefile, char *nickOrigen,  int dim, char* prefix){
	/*Mensaje que recibe el que va a enviar el mensaje*/
	char command_send_aux[DIM_FSEND];
	if(msg == NULL){
		return -1;
	}
	if(nickDest == NULL || namefile == NULL || nickOrigen == NULL || dim <= 0){
		return -2;
	}
	/*Limpiar cadena*/
	bzero(command_send_aux,DIM_FSEND);

	/*Crear /fsend <namefile> <nickOrigen> <dim>*/
	sprintf(command_send_aux, "/fsend %s %s %d", namefile, nickOrigen, dim);

	/*Enmascarar en un PrivMSG*/
	if(IRCMsg_Privmsg (msg, prefix, nickDest, command_send_aux) != IRC_OK){
		return -3;
	}

	return 0;
}

int getDimOfFile(char * path){
	int size = 0;
	FILE * file = NULL;

	if(path == NULL){
		return -1;
	}

	if((file = fopen(path, "r")) == NULL){
		return -2;
	} /*Abir fichero*/

	/*Posicionarse al final del fichero*/
	fseek(file, 0L, SEEK_END);
	/*Obtener posicion*/
	size = ftell(file);

	/*Cerrar fichero*/
	fclose(file);
	return size;
}

int send_file(char * ip, int port, char* pathfile){
	int dim = 0;
	int socket_id = 0;
	int sockfd = 0;
	int desc_file = 0;
	char * msgFile = NULL;
	struct sockaddr_in addr;

	if(ip == NULL || port <= 0 || pathfile == NULL){
		return -1;
	}

	/*Dim*/
	if((dim = getDimOfFile(pathfile)) <= 0){
		return -2;
	}

	/*Reserva memoria*/
	if((msgFile = (char *) calloc(dim+2, sizeof(char)))== NULL){
		return -3;
	}

	/*Abrir fichero*/
	if((desc_file = open(pathfile, O_RDONLY)) < 0){
		return -4;
	}

	/*Leer fichero*/
	if(read(desc_file, msgFile, (dim+1)*sizeof(char)) < 0){
		return -5;
	} 

	/*Cerrar fichero*/
	close(desc_file);
	desc_file = 0;
	
	/*Create socket_id*/
	if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        /*Cuando ejecutes con daemon no tienes printf, usa syslog*/
        printMsg("Error en socket_id\n", LOG_ERR, 1);
        return -6;
    }


    /*Rellenar la estructura*/
    bzero((char *) &addr, sizeof(addr));
    addr.sin_addr.s_addr = inet_addr(ip);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);


    /*Connect*/
    if (connect(sockfd, (struct sockaddr *) &addr, sizeof(addr)) < 0){
    	printMsg("Error to connect\n", LOG_ERR, 1);
	    return -7;
    }


    write(sockfd, msgFile, (dim+1)*sizeof(char));


    close(sockfd);
    close(socket_id);
    free(msgFile);
    msgFile = NULL;
    return 0;
}

int read_file(int port, char * pathfile, int dim){
	int socket_id = 0;
	int socketfd = 0;
	int desc_file = 0;
	char * msgFile = NULL;
	struct sockaddr addr;
	int size = 0;

	/*Abrir puerto*/
    if((socket_id = socket_create(AF_INET,SOCK_STREAM,port))==-1){
        printMsg("Error to create socket \n", LOG_ERR, 1);
        return -1;
    }

	/*Limpiamos los datos a enviar*/
	bzero(&addr, sizeof(struct sockaddr));

	/*Aceptamos la conexion (Espera)*/
	size = sizeof(addr);
	if((socketfd = accept(socket_id, &addr, (socklen_t *) &size)) == -1){
		printMsg("Connection refused\n", LOG_ERR, 1);
		close(socket_id);
		return -2;
	}

	/*Reserva memoria*/
	if((msgFile = (char *) calloc(dim+2, sizeof(char)))== NULL){
		close(socket_id);
		close(socketfd);
		return -3;
	}

	/*Le el fichero de la red*/
	if(read(socketfd,  msgFile, (dim+1)*sizeof(char)) <= 0){
		close(socket_id);
		close(socketfd);
		free(msgFile);
		return -4;
	}
	
	/*Abrir fichero*/
	if((desc_file = open(pathfile, O_RDWR | O_CREAT | O_TRUNC, 0600)) < 0){
		close(socket_id);
		close(socketfd);
		free(msgFile);
		return -5;
	}

	/*Escribe el dichero en disco*/
	write(socketfd, msgFile, (dim+1)*sizeof(char));

	close(socket_id);
	close(socketfd);
	free(msgFile);
	return 0;
}


/* Protocolo 
		El que envia
		-> /fsend <ruta> <nickDest>
			lo escribe el usuario
			-> /fsend <namefile> <nickOrigen> <dim>
				lo genera el programa

		<A- /accept <ip> <port>
			se conecta al puerto con esa ip (connect)
			lee el fichero
			y lo envia.

		<B- /cancel
			no hace nada

	El que recibe
		<- /fsend <namefile> <nickOrigen> <dim>
			Se le inidica

		-A> /accept <ip> <port>
			ip y port lo elige el sistem
			crea una cadena para recibir el mensaje
			abre el puerto 
			envia el accept
			espera a la conexion
			cuando ha recibido escribe en fichero

		-B> /cancel
			no hace nada
*/