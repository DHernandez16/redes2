/**
 * @file irc_auxFunctions.h 
 * @brief general auxiliar Funcions to help
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRC_AUXFUNCTIONS
#define IRC_AUXFUNCTIONS

#include "irc_client.h"

/**
 * @function strclone
 * @date 2017/2/9
 * @brief Clona una cadena
 * @param str cadena a copiar 
 * @return string created
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
char * strclone(char * str);

/**
 * @function printMsg
 * @date 2017/2/9
 * @brief deamonizes caller process
 * @param char * msg: 
 * @param int log_mode: 
 * @param int perr: 
 * @author: Miguel Angel Alvarez Rodriguez
 * @verison: 1.1
 */
void printMsg(char *msg, int log_mode, int perr);

/**
 * @function CharMalloc
 * @date 2017/2/9
 * @brief Create a string of dimMAXBYTES
 * @return int: String created
 * @author: Dario Adrian Hernandez Barroso
 * @version: 1.2
 */
char* CharMalloc ();

#endif /*IRC_AUXFUNCTIONS*/
