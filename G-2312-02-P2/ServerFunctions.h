/**
 * @file ServerFunctions.h 
 * @brief Handel commands of server
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef SERVERFUNCTIONS
#define SERVERFUNCTIONS

#define DIM_SERVERUFUNS 200

/*Array para las funciones cuando recibes cosas del servidor*/
int (*serverpfuns[DIM_SERVERUFUNS])(char*);


void addPServerFunToArray(int pos, int (*fun)(char*));

void inicializatePServerFuns(int (*fun)(char*));

int FunServerDefault (char* str);

int ServerJoin (char* str);

#endif /* SERVERFUNCTIONS */

