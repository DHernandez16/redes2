
/**
 * @file sendfile.h 
 * @brief Functions to manage protocol to sendfiles

 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/5/5
 * @version 1.0
 */

#ifndef SENDFILE
#define SENDFILE
#include "irc_client.h"
#include "irc_auxFunctions.h"

#define DIM_FSEND 50


int IRCParse_faccept(char ** ip, int * port);

int IRCMSG_faccept(char ** msg, char *nickDest, char* ip, int port, char* prefix);

int IRCMSG_fsend(char ** msg, char *nickDest, char* namefile, char *nickOrigen,  int dim, char* prefix);

int getDimOfFile(char * path);

int send_file(char * ip, int port, char* pathfile);

int read_file(int port, char * pathfile, int dim);



#endif /* SENDFILE */



/* Protocolo 
		El que envia
		-> /fsend <ruta> <nickDest>
			lo escribe el usuario
			-> /fsend <namefile> <nickOrigen> <dim>
				lo genera el programa

		<A- /accept <ip> <port>
			se conecta al puerto con esa ip (connect)
			lee el fichero
			y lo envia.

		<B- /cancel
			no hace nada

	El que recibe
		<- /fsend <namefile> <nickOrigen> <dim>
			Se le inidica

		-A> /accept <ip> <port>
			ip y port lo elige el sistem
			crea una cadena para recibir el mensaje
			abre el puerto 
			envia el accept
			espera a la conexion
			cuando ha recibido escribe en fichero

		-B> /cancel
			no hace nada
*/