/**
 * @file irc_client.h 
 * @brief defines constants and includes 

 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef IRC_CLIENT
#define IRC_CLIENT
#include <errno.h>
#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <redes2/irc.h>
#include <redes2/ircxchat.h>


/*DIM CONSTANTS*/
#define BYTES_READ 512
#define DIM_NAME_SYSLOG 64
#define DIM_MSG  250
#define MAXBYTES 512 /*bytes máximos que pueden ser enviados en un paquete irc*/


/*Configuration*/
#define SOCKET_PORT 6667
#define SECONDS_TO_PONG 30
#define DEBUG_LOG_MODE LOG_INFO

/*Debug*/
#define DEBUG_PRINT 

#endif /* IRC_CLIENT */

