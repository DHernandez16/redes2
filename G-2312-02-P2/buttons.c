/**
 * @file buttons.c
 * @brief Handel Callback of buttons
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

//#include "UFunctions.h"
#include "ircGeneral.h"
#include "irc_auxFunctions.h"
#include "irc_client.h"

#include "buttons.h"

 
void IRCInterface_ActivateChannelKey(char *channel, char *key){
	char* msg;
	IRCMsg_Mode (&msg, cln.prefix, channel, "+k", key);
	SendToServer(msg);
	free(msg);

	/*msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateChannelKey.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +k %s\n\rMODE %s\n\r", channel, key, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateChannelKey.", LOG_ERR,1);
		return;
	}

	printMsg("\nChannel keyword key pushed.", LOG_INFO,0);

	free(msg);*/

}

 
void IRCInterface_ActivateExternalMessages(char *channel){
	char* msg;
	IRCMsg_Mode (&msg, cln.prefix, channel, "+n", NULL);
	SendToServer(msg);
	free(msg);
	/*msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateExternalMessages.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +n\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateExternalMessages.", LOG_ERR,1);
		return;
	}

	printMsg("\nExternal messages key pushed.", LOG_INFO,0);

	free(msg);*/
}

 
void IRCInterface_ActivateInvite(char *channel){
	char* msg;
	IRCMsg_Mode (&msg, cln.prefix, channel, "+i", NULL);
	SendToServer(msg);
	free(msg);
	/*msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateInvite.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +i\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateInvite.", LOG_ERR,1);
		return;
	}

	printMsg("\nInvite key pushed.", LOG_INFO,0);

	free(msg);*/
}

 
void IRCInterface_ActivateModerated(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateModerated.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +m\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateModerated.", LOG_ERR,1);
		return;
	}

	printMsg("\nModerated key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_ActivateNicksLimit(char *channel, int limit){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateNicksLimit.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s %d +m\n\rMODE %s\n\r", channel, limit,channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_Ac.", LOG_ERR,1);
		return;
	}

	printMsg("\n Limit key pushed.", LOG_INFO,0);

	free(msg);

}

 
void IRCInterface_ActivatePrivate(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivatePrivate.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +p\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivatePrivate.", LOG_ERR,1);
		return;
	}

	printMsg("\nPrivate key pushed.", LOG_INFO,0);

	free(msg);
	
}


void IRCInterface_ActivateProtectTopic(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateProtectTopic.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +t\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateProtectTopic.", LOG_ERR,1);
		return;
	}

	printMsg("\nProtect topic key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_ActivateSecret(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateSecret.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +s\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateSecret.", LOG_ERR,1);
		return;
	}

	printMsg("\nSecret key pushed.", LOG_INFO,0);

	free(msg);
}


 
void IRCInterface_BanNick(char *channel, char *nick){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_BanNick.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +b %s\n\rMODE %s\n\r", channel, nick, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_BanNick.", LOG_ERR,1);
		return;
	}

	printMsg("\nBan (OP) key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_DeactivateChannelKey(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateChannelKey.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -k\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateChannelKey.", LOG_ERR,1);
		return;
	}

	printMsg("\nChannel keyword key pushed.", LOG_INFO,0);

	free(msg);
}

void IRCInterface_DeactivateExternalMessages(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateExternalMessages.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -n\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateExternalMessages.", LOG_ERR,1);
		return;
	}

	printMsg("\nExternal messages key pushed.", LOG_INFO,0);

	free(msg);
}


void IRCInterface_DeactivateInvite(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateInvite.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -i\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateInvite.", LOG_ERR,1);
		return;
	}

	printMsg("\nInvite key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_DeactivateModerated(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateModerated.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -m\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateModerated.", LOG_ERR,1);
		return;
	}

	printMsg("\nModerated key pushed.", LOG_INFO,0);

	free(msg);
}

void IRCInterface_DeactivateNicksLimit(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateNicksLimit.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -l\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateNicksLimit.", LOG_ERR,1);
		return;
	}

	printMsg("\nLimit key pushed.", LOG_INFO,0);

	free(msg);
}

void IRCInterface_DeactivatePrivate(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivatePrivate.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -p\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivatePrivate.", LOG_ERR,1);
		return;
	}

	printMsg("\nPrivate key pushed.", LOG_INFO,0);

	free(msg);
	
}

void IRCInterface_DeactivateProtectTopic(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_ActivateProtectTopic.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -t\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_ActivateProtectTopic.", LOG_ERR,1);
		return;
	}

	printMsg("\nProtect topic key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_DeactivateSecret(char *channel){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_DeactivateSecret.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -s\n\rMODE %s\n\r", channel, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_DeactivateSecret.", LOG_ERR,1);
		return;
	}

	printMsg("\nSecret key pushed.", LOG_INFO,0);

	free(msg);
}

 
boolean IRCInterface_DisconnectServer(char *server, int port){

	printMsg("Desconectando del server.", LOG_INFO,0);

	close(cln.server_socket);

	return TRUE;
}
 
void IRCInterface_GiveOp(char *channel, char *nick){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_GiveOp.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s +o %s\n\rMODE %s\n\r", channel, nick,channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_GiveOp.", LOG_ERR,1);
		return;
	}

	printMsg("\nGive Op key pushed.", LOG_INFO,0);

	free(msg);
}

 
void IRCInterface_GiveVoice(char *channel, char *nick)
{
	//@@@@@@@@@@@@@@@@@@ EHHHH QUE NO ME HAS HECHO
}

 
void IRCInterface_KickNick(char *channel, char *nick){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_KickNick.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "KICK %s %s\n\r", channel, nick);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_KickNick.", LOG_ERR,1);
		return;
	}

	printMsg("\nKick key pushed.", LOG_INFO,0);

	free(msg);
}


 
void IRCInterface_NewTopicEnter(char *topicdata){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_NewTopicEnter.", LOG_ERR,1);
		return;
	}

	//sprintf(msg, "TOPIC %s :%s\n\r", IRcInterface_ActiveChannelName(), topicdata);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_NewTopicEnter.", LOG_ERR,1);
		return;
	}

	printMsg("\nTOPIC sent to server.", LOG_INFO,0);

	free(msg);
}

void IRCInterface_TakeOp(char *channel, char *nick){
	char* msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @IRCInterface_GiveOp.", LOG_ERR,1);
		return;
	}

	sprintf(msg, "MODE %s -o %s\n\rMODE %s\n\r", channel, nick,channel);
	IRCInterface_PlaneRegisterOutMessage(msg);

	if (SendToServer(msg) < 0){
		printMsg("\nError, send to server failed @IRCInterface_GiveOp.", LOG_ERR,1);
		return;
	}

	printMsg("\nGive Op key pushed.", LOG_INFO,0);

	free(msg);
}
 
void IRCInterface_TakeVoice(char *channel, char *nick){
}