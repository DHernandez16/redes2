#include "irc_client.h"
#include "irc_auxFunctions.h"
#include "ircGeneral.h"
#include "UFunctions.h"
#include "ServerFunctions.h"
#include "buttons.h"



void* ListenFromServer (void * desc);

/** 
 * @defgroup IRCInterface Interface
 *
 */


/*Funcion que se encarga de leer todo lo que le manda el servidor, */
void* ListenFromServer (void * desc) {
	int bytesread;
	char str_read[MAXBYTES];

	while(1){
		bzero(str_read, BYTES_READ);
        bytesread = read(cln.server_socket, str_read, BYTES_READ);
        if (bytesread == 0) {
            break;
        }
        IRCInterface_PlaneRegisterInMessageThread (str_read);
        /*@@@@@@@@@@@@@@@@@2hacer la llamada a la funcion correspondiente*/
	}

	printMsg("Finalizando ejecucion del hilo\n", LOG_INFO, 0);
    pthread_detach(pthread_self());

    return NULL;
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_NewCommandText IRCInterface_NewCommandText
 *
 * @brief Llamada la tecla ENTER en el campo de texto y comandos.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_NewCommandText (char *command)
 * @endcode
 * 
 * @description
 * Llamada de la tecla ENTER en el campo de texto y comandos. El texto deberá ser
 * enviado y el comando procesado por las funciones de "parseo" de comandos de
 * usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * La string recibida no debe ser manipulada por el programador, sólo leída.
 *
 * @param[in] comando introducido por el usuario.
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/
void IRCInterface_NewCommandText(char *str){
	
	long command = IRCUser_CommandQuery(str);
	
	if (command == IRCERR_NOSTRING){
		printMsg("\nError, invalid string @IRCInterface_NewCommandText", LOG_ERR,1);
		return;
	}

	if (command == IRCERR_NOUSERCOMMAND || command == IRCERR_NOVALIDUSERCOMMAND){
		//IRCInterface_WriteChannel(IRcInterface_ActiveChannelName(), cln.nick, str);
		printMsg(str, LOG_INFO, 0);
		return;
	}

	if (command <= UWHO){
		if (pfuns[command](str) != 0)
			printMsg("\nError, function command returned != 0 @IRCInterface_NewCommandText", LOG_ERR, 1);
	}else
		printMsg("\nCommand not implemented.", LOG_INFO, 0);

}

//@@@@@@@ HEEE QUE ME FALTA LA DESCRIPCION
boolean IRCInterface_SendFile(char *filename, char *nick, char *data, long unsigned int length){
	return TRUE;
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_StartAudioChat IRCInterface_StartAudioChat
 *
 * @brief Llamada por el botón "Iniciar" del diálogo de chat de voz.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_StartAudioChat (char *nick)
 * @endcode
 * 
 * @description 
 * Llamada por el botón "Iniciar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función como todos los demás callbacks bloquea el interface
 * y por tanto para mantener la funcionalidad del chat de voz es imprescindible crear un hilo a efectos
 * de comunicación de voz.
 * 
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * La string recibida no debe ser manipulada por el programador, sólo leída.
 *
 * @param[in] nick nick del usuario con el que se desea conectar.
 *
 * @retval TRUE si se ha establecido la comunicación (debe devolverlo).
 * @retval FALSE en caso contrario (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/
 
boolean IRCInterface_StartAudioChat(char *nick){
	return TRUE;
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ExitAudioChat IRCInterface_ExitAudioChat
 *
 * @brief Llamada por el botón "Cancelar" del diálogo de chat de voz.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ExitAudioChat (char *nick)
 * @endcode
 * 
 * @description 
 * Llamada por el botón "Parar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función cierrala comunicación. Evidentemente tiene que
 * actuar sobre el hilo de chat de voz.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * La string recibida no debe ser manipulada por el programador, sólo leída.
 *
 * @param[in] nick nick del usuario que solicita la parada del chat de audio.
 *
 * @retval TRUE si se ha cerrado la comunicación (debe devolverlo).
 * @retval FALSE en caso contrario (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/
 
boolean IRCInterface_ExitAudioChat(char *nick)
{
	return TRUE;
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_StopAudioChat IRCInterface_StopAudioChat
 *
 * @brief Llamada por el botón "Parar" del diálogo de chat de voz.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_StopAudioChat (char *nick)
 * @endcode
 * 
 * @description 
 * Llamada por el botón "Parar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función sólo para la comunicación que puede ser reiniciada. 
 * Evidentemente tiene que actuar sobre el hilo de chat de voz.
 * 
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * La string recibida no debe ser manipulada por el programador, sólo leída.
 *
 * @param[in] nick nick del usuario con el que se quiere parar el chat de voz.
 *
 * @retval TRUE si se ha parado la comunicación (debe devolverlo).
 * @retval FALSE en caso contrario (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/
 
boolean IRCInterface_StopAudioChat(char *nick){
	return TRUE;
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_Connect IRCInterface_Connect
 *
 * @brief Llamada por los distintos botones de conexión.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	long IRCInterface_Connect (char *nick, char * user, char * realname, char * password, char * server, int port, boolean ssl)
 * @endcode
 * 
 * @description 
 * Función a implementar por el programador.
 * Llamada por los distintos botones de conexión. Si implementará la comunicación completa, incluido
 * el registro del usuario en el servidor.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * Las strings recibidas no deben ser manipuladas por el programador, sólo leída.
 *
 *
 * @param[in] nick nick con el que se va a realizar la conexíón.
 * @param[in] user usuario con el que se va a realizar la conexión.
 * @param[in] realname nombre real con el que se va a realizar la conexión.
 * @param[in] password password del usuario si es necesaria, puede valer NULL.
 * @param[in] server nombre o ip del servidor con el que se va a realizar la conexión.
 * @param[in] port puerto del servidor con el que se va a realizar la conexión.
 * @param[in] ssl puede ser TRUE si la conexión tiene que ser segura y FALSE si no es así.
 *
 * @retval IRC_OK si todo ha sido correcto (debe devolverlo).
 * @retval IRCERR_NOSSL si el valor de SSL es TRUE y no se puede activar la conexión SSL pero sí una 
 * conexión no protegida (debe devolverlo).
 * @retval IRCERR_NOCONNECT en caso de que no se pueda realizar la comunicación (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

long IRCInterface_Connect(char *nick, char *user, char *realname, char *password, char *server, int port, boolean ssl)
{
	int cln_socket;
	char* msg = NULL;
	struct sockaddr_in addr;
	struct hostent* host;
	pthread_t listener;

	/*Comprobamos argumentos obligatorios*/
	if (!nick || !user || !realname || !server || port < 0){
		printMsg("\nError, invalid arguments @IRCInterface_Connect.", LOG_ERR, 1);
		return IRCERR_NOCONNECT;
	}

	/*Socket del cliente*/
	cln_socket = socket(AF_INET,SOCK_STREAM, 0);
	if (cln_socket == -1){
		printMsg("\nError, unable to create socket @IRCInterface_Connect.", LOG_ERR, 1);
		return IRCERR_NOCONNECT;
	}

	/*Sacamos una estructura de tipo hostent con:
		char* h_name: nombre 
		char** aliases: nombres alternativos del host
		int h_addrtype: tipo de dirección, normalmente AF_INET.
		int h_length: bytes de cada dirección.
		char** h_addr_list: vector con las direcciones del host.
		char* h_addr: primer valor del vector anteriormente mencionado.
	*/
	host = gethostbyname(server);
	if (!host){
		printMsg("\nError, unable to gethostbyname @IRCInterface_Connect.", LOG_ERR, 1);
		return IRCERR_NOCONNECT;
	}

	/*Conexión con el servidor*/
	addr.sin_family = AF_INET; 
	addr.sin_port = htons(port); 
	addr.sin_addr = *((struct in_addr *)host->h_addr);
	if (connect(cln_socket,(struct sockaddr *)&addr,sizeof(addr)) != 0){
		printMsg("\nError, unable to connect to server @IRCInterface_Connect.", LOG_ERR, 1);
		return IRCERR_NOCONNECT;
	}

	/*Abrimos un hilo que se encargue de escuchar lo que llega del servidor*/
	if (pthread_create(&listener, NULL, &ListenFromServer, NULL) != 0) {
        printMsg("Error to create a threat\n", LOG_ERR, 0);
        return IRCERR_NOCONNECT;
    }

	/*Dejamos la información que nos hará falta más adelante en la estructura global*/
	cln.server_socket = cln_socket;
	cln.nick = nick;
	//@@@@@@@@@@@@@@@@@@@@@ CREAR EL PREFIX
	cln.prefix = NULL;

	printf("Socket : %d\n", cln.server_socket);

	/*Mandamos los mensajes de conexión al servidor*/
	/*Mandar password primero si existe*/
	if(strcmp(password, "") != 0){
		if(IRCMsg_Pass (&msg, cln.prefix, password) != IRC_OK){
			return IRCERR_NOCONNECT;
		}
		SendToServer(msg);
		free(msg);
		msg = NULL;
		printMsg("\nPASS sent to server\n", LOG_INFO, 0);
	}

	/*Mandamos el nick y el user*/
	if(IRCMsg_Nick (&msg, cln.prefix, nick, NULL) == IRC_OK){
		SendToServer(msg);
		free(msg);
		msg = NULL;
	}else{
		return IRCERR_NOCONNECT;
	}

	if(IRCMsg_User (&msg, cln.prefix, user, "", realname) == IRC_OK){
		SendToServer(msg);
		free(msg);
		msg = NULL;
	}else{
		return IRCERR_NOCONNECT;
	}

	printMsg("\nNICK & USER sent to server\n", LOG_INFO, 0);

	return IRC_OK;
}

/***************************************************************************************************/
/***************************************************************************************************/
/**                                                                                               **/
/** MMMMMMMMMM               MMMMM           AAAAAAA           IIIIIII NNNNNNNNNN          NNNNNN **/
/**  MMMMMMMMMM             MMMMM            AAAAAAAA           IIIII   NNNNNNNNNN          NNNN  **/
/**   MMMMM MMMM           MM MM            AAAAA   AA           III     NNNNN NNNN          NN   **/
/**   MMMMM  MMMM         MM  MM            AAAAA   AA           III     NNNNN  NNNN         NN   **/
/**   MMMMM   MMMM       MM   MM           AAAAA     AA          III     NNNNN   NNNN        NN   **/
/**   MMMMM    MMMM     MM    MM           AAAAA     AA          III     NNNNN    NNNN       NN   **/
/**   MMMMM     MMMM   MM     MM          AAAAA       AA         III     NNNNN     NNNN      NN   **/
/**   MMMMM      MMMM MM      MM          AAAAAAAAAAAAAA         III     NNNNN      NNNN     NN   **/
/**   MMMMM       MMMMM       MM         AAAAA         AA        III     NNNNN       NNNN    NN   **/
/**   MMMMM        MMM        MM         AAAAA         AA        III     NNNNN        NNNN   NN   **/
/**   MMMMM                   MM        AAAAA           AA       III     NNNNN         NNNN  NN   **/
/**   MMMMM                   MM        AAAAA           AA       III     NNNNN          NNNN NN   **/
/**  MMMMMMM                 MMMM     AAAAAA            AAAA    IIIII   NNNNNN           NNNNNNN  **/
/** MMMMMMMMM               MMMMMM  AAAAAAAA           AAAAAA  IIIIIII NNNNNNN            NNNNNNN **/
/**                                                                                               **/
/***************************************************************************************************/
/***************************************************************************************************/



int main (int argc, char *argv[])
{
	/* La función IRCInterface_Run debe ser llamada al final      */
	/* del main y es la que activa el interfaz gráfico quedándose */
	/* en esta función hasta que se pulsa alguna salida del       */
	/* interfaz gráfico.                                          */

	/*Inicializacion*/
	inicializatePUFuns(FunUDefault);
	inicializatePServerFuns(FunServerDefault);

	/*Graficas*/
	IRCInterface_Run(argc, argv);

	return 0;
}
