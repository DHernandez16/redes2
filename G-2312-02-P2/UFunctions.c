/**
 * @file UFunctions.c
 * @brief Handel commands of user
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "UFunctions.h"
#include "ircGeneral.h"
#include "irc_auxFunctions.h"
#include "irc_client.h"

void addPUFunToArray(int pos, int (* fun)(char*)){
	if(pos >= 0 && pos < DIM_PUFUNS){
		pfuns[pos] = fun;
	}
}

void inicializatePUFuns(int (*def_fun)(char*)){
	int i; 
	for(i=0; i< DIM_PUFUNS; i++){
		pfuns[i] = def_fun;
	}

	addPUFunToArray(UNAMES, FunUnames);
	addPUFunToArray(UHELP, FunUhelp);
	addPUFunToArray(ULIST, FunUlist);
	addPUFunToArray(UJOIN, FunUjoin); /*OK*/
	addPUFunToArray(UPART, FunUpart);
	addPUFunToArray(UQUIT, FunUquit);
	addPUFunToArray(UNICK, FunUnick);
	addPUFunToArray(UAWAY, FunUaway);
	addPUFunToArray(UWHOIS, FunUwhois);
	addPUFunToArray(UINVITE, FunUinvite);
	addPUFunToArray(UKICK, FunUkick);
	addPUFunToArray(UTOPIC, FunUtopic);
	addPUFunToArray(UNAMES, FunUnames);
	addPUFunToArray(UME, FunUme);
	addPUFunToArray(UMSG, FunUmsg);
	addPUFunToArray(UQUERY, FunUquery);
	addPUFunToArray(UNOTICE, FunUnotice);
	addPUFunToArray(UNOTIFY, FunUnotify);
	addPUFunToArray(UIGNORE, FunUignore);
	addPUFunToArray(UPING, FunUping);
	addPUFunToArray(UWHO, FunUwho);
	//addPUFunToArray(0, AAAAA);
}

int FunUDefault (char* str){
	printMsg("UCommand not handel", LOG_NOTICE, 0);
	printMsg(str, LOG_NOTICE, 0);
	return 0;
}

int FunUnames (char* str){
	char* channel, *msg, *targetserver;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUhelp.", LOG_ERR, 1);
		return -1;	
	}

	/*Pasamos el parse_names, pero no sé que hace la función __uPO__*/
	if (IRCUserParse_Names(str, &channel, &targetserver) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUNames.", LOG_ERR, 1);
		return -1;
	}
	
	/*Creamos el mensaje para el servidor y se lo mandamos*/
	sprintf(msg, "NAMES %s\r\n", channel);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUnames.", LOG_ERR, 1);
		return -1;
	}	
	printMsg("\nNAMES sent to server.", LOG_INFO, 0);

	/*Mandamos el mismo mensaje al registro plano*/
	IRCInterface_PlaneRegisterOutMessage(msg);

	free(msg);
	if (channel) 
		free(channel);
	if (targetserver) 
		free(targetserver);

	return 0;
}

int FunUhelp (char* str){
	char* command, *msg;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUhelp.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Help(str, &command) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUhelp.", LOG_ERR, 1);
		return -1;
	}

	/*Creamos el mensaje para el servidor y se lo mandamos*/
	sprintf(msg, "HELP %s\r\n", command);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUhelp.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nHELP sent to server.", LOG_INFO, 0);

	free(msg);
	if (command) 
		free(command);
	
	return 0;
}

int FunUlist (char* str){
	char* msg, *channel, *searchstring;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUlist.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_List(str, &channel, &searchstring) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUlist.", LOG_ERR, 1);
		return -1;
	}

	/*Creamos el mensaje para el servidor y se lo mandamos*/
	sprintf(msg, "LIST %s\r\n", searchstring);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUlist.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nHELP sent to server.", LOG_INFO, 0);

	IRCInterface_PlaneRegisterOutMessage(msg);

	free(msg);
	if (channel) 
		free(channel);
	if (searchstring) 
		free(searchstring);
	
	return 0;
}

int FunUjoin (char* str){
	char* msg = NULL, *channel = NULL, *password = NULL;
	char *aux = NULL;

	if (IRCUserParse_Join(str, &channel, &password) != IRC_OK){
		aux = IRCInterface_ActiveChannelName ();
		if(aux == NULL || strcmp(aux, "System") == 0){
			IRCInterface_WriteSystem (NULL, "Usage: JOIN <channel>, joins the channel");
		}else{
			IRCInterface_WriteChannel (aux, NULL, "Usage: JOIN <channel>, joins the channel");
		}
		return 0;
	}	

	if(IRCMsg_Join (&msg, cln.prefix, channel, password, "Holaaa") != IRC_OK){
		return -2;
	}

	if (SendToServer(msg) < 0){
		printMsg("Error, SendToServer failed @FunUjoin.\n", LOG_ERR, 1);
		return -3;
	}
	printMsg("JOIN sent to server.\n", LOG_INFO, 0);

	free(msg); 
	free(channel);
	free(password);

	return 0;
}

int FunUpart (char* str){
	char* msg, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUpart.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Part(str, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUpart.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "PART %s\r\n", message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUpart.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nPART sent to server.", LOG_INFO, 0);

	free(msg);
	if (message)
		free(message);

	return 0;
}

/*En la página está fatal, la idea es que el mensaje es [/leave #channel], source: http://www.livinginternet.com/r/ru_join.htm*/
int FunUleave (char* str){
	char* msg, *channel;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUleave.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Leave(str, &channel) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUleave.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "LEAVE %s\r\n", channel);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUleave.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nLEAVE sent to server.", LOG_INFO, 0);

	free(msg);
	if (channel)
		free(channel);

	return 0;
}

int FunUquit (char* str){
	char* msg, *reason;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUquit.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Quit(str, &reason) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUquit.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "QUIT %s\r\n", reason);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUquit.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nQUIT sent to server.", LOG_INFO, 0);

	free(msg);
	if (reason)
		free(reason);

	return 0;
}

int FunUnick (char* str){
	char* msg, *newNick;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUnick.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Nick(str, &newNick) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUnick.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "Nick %s\r\n", newNick);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUnick.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nNick sent to server.", LOG_INFO, 0);

	free(msg);
	if (newNick)
		free(newNick);

	return 0;
}

int FunUaway (char* str){
	char* msg, *reason;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUaway.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Away(str, &reason) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUaway.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "AWAY %s\r\n", reason);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUaway.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nAWAY sent to server.", LOG_INFO, 0);

	free(msg);
	if (reason)
		free(reason);

	return 0;
}

int FunUwhois (char* str){
	char* msg, *nick;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUwhois.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Whois(str, &nick) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUwhois.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "WHOIS %s\r\n", nick);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUwhois.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nWHOIS sent to server.", LOG_INFO, 0);

	free(msg);
	if (nick)
		free(nick);

	return 0;
}

int FunUinvite (char* str){
	char* msg, *nick, *channel;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUinvite.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Invite(str, &nick, &channel) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUinvite.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "INVITE %s %s\r\n", nick, channel);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUinvite.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nINVITE sent to server.", LOG_INFO, 0);

	free(msg);
	if (nick)
		free(nick);
	if (channel)
		free(channel);

	return 0;
}

int FunUkick (char* str){
	char* msg, *nick, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUkick.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Kick(str, &nick, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUkick.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "KICK %s %s\r\n", nick, message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUkick.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nKICK sent to server.", LOG_INFO, 0);

	free(msg);
	if (nick)
		free(nick);
	if (message)
		free(message);

	return 0;
}

int FunUtopic (char* str){
	char* msg, *topic;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUtopic.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Topic(str, &topic) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUtopic.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "TOPIC %s\r\n", topic);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUtopic.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nTOPIC sent to server.", LOG_INFO, 0);

	free(msg);
	if (topic)
		free(topic);

	return 0;
}

int FunUme (char* str){
	char* msg, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUme.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Me(str, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUme.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "ME %s\r\n", message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUme.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nME sent to server.", LOG_INFO, 0);

	free(msg);
	if (message)
		free(message);

	return 0;
}

int FunUmsg (char* str){
	char* msg, *nickOrChannel, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUmsg.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Msg(str, &nickOrChannel, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUmsg.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "MSG %s %s\r\n", nickOrChannel, message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUmsg.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nMSG sent to server.", LOG_INFO, 0);

	free(msg);
	if (nickOrChannel)
		free(nickOrChannel);
	if (message)
		free(message);

	return 0;
}

int FunUquery (char* str){
	char* msg, *nickOrChannel, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUquery.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Query(str, &nickOrChannel, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUquery.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "QUERY %s %s\r\n", nickOrChannel, message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUquery.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nQUERY sent to server.", LOG_INFO, 0);

	free(msg);
	if (nickOrChannel)
		free(nickOrChannel);
	if (message)
		free(message);

	return 0;
}

int FunUnotice (char* str){
	char* msg, *nickOrChannel, *message;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUnotice.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Notice(str, &nickOrChannel, &message) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUnotice.", LOG_ERR, 1);
		return -1;
	}		

	sprintf(msg, "NOTICE %s %s\r\n", nickOrChannel,message);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUnotice.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nNOTICE sent to server.", LOG_INFO, 0);

	free(msg);
	if (message)
		free(message);
	if (nickOrChannel)
		free(nickOrChannel);

	return 0;
}

int FunUnotify (char* str){
	char* msg, *nickaux;
	char** nickarray;
	int numnicks, i;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUnotify.", LOG_ERR, 1);
		return -1;
	}

	nickaux = CharMalloc();
	if (!nickaux){
		printMsg("\nError, no memmory for malloc @FunUnotify.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Notify(str, &nickarray, &numnicks) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUnotify.", LOG_ERR, 1);
		return -1;
	}

	sprintf(msg, "NOTIFY");
	/*Espero que este puesto para que numnicks no se cuente desde el 0. Ej: Hay 3 manzanas, por lo que cuento de la manzana[0] a manzana[2]*/
	for (i = 0; i < numnicks; i++){
		sprintf(nickaux, " %s", nickarray[i]);
		strcat(msg, nickaux);
	}
	strcat(msg, "\r\n"); /*Ponemos el \r\n al final de la cadena como en el resto de comandos*/
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUnotify.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nNOTICE sent to server.", LOG_INFO, 0);
	/*Enviamos además el mensaje creado como extra, por si aca*/
	printMsg(msg, LOG_INFO, 0);


	/*Liberamos recursos*/
	IRC_MFree(2, msg, nickaux);
	for (i = 0; i < numnicks; i++){
		free(nickarray[i]);
	}
	free(nickarray);

	return 0;
}

int FunUignore (char* str){
	char* msg, *nickuserhostarrayAUX;
	char** nickuserhostarray;
	int numnickuserhost, i;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUignore.", LOG_ERR, 1);
		return -1;
	}

	nickuserhostarrayAUX = CharMalloc();
	if (!nickuserhostarrayAUX){
		printMsg("\nError, no memmory for malloc @FunUignore.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Ignore(str, &nickuserhostarray, &numnickuserhost) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUignore.", LOG_ERR, 1);
		return -1;
	}

	sprintf(msg, "IGNORE");
	/*Espero que este puesto para que numnickuserhost no se cuente desde el 0. Ej: Hay 3 manzanas, por lo que cuento de la manzana[0] a manzana[2]*/
	for (i = 0; i < numnickuserhost; i++){
		sprintf(nickuserhostarrayAUX, " %s", nickuserhostarray[i]);
		strcat(msg, nickuserhostarrayAUX);
	}
	strcat(msg, "\r\n"); /*Ponemos el \r\n al final de la cadena como en el resto de comandos*/
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUignore.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nIGNORE sent to server.", LOG_INFO, 0);
	/*Enviamos además el mensaje creado como extra, por si aca*/
	printMsg(msg, LOG_INFO, 0);


	/*Liberamos recursos*/
	IRC_MFree(2, msg, nickuserhostarrayAUX);
	for (i = 0; i < numnickuserhost; i++){
		free(nickuserhostarray[i]);
	}
	free(nickuserhostarray);

	return 0;
}

int FunUping (char* str){
	char* msg, *user;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUping.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Ping(str, &user) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUping.", LOG_ERR, 1);
		return -1;
	}

	sprintf(msg, "PING %s\r\n", user);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUping.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nPING sent to server.", LOG_INFO, 0);

	free(msg);
	if (user)
		free(user);

	return 0;
}

int FunUwho (char* str){
	char* msg, *mask;

	msg = CharMalloc();
	if (!msg){
		printMsg("\nError, no memmory for malloc @FunUwho.", LOG_ERR, 1);
		return -1;
	}

	if (IRCUserParse_Who(str, &mask) != IRC_OK){
		printMsg("\nError, no IRC_OK @FunUwho.", LOG_ERR, 1);
		return -1;
	}

	sprintf(msg, "WHO %s\r\n", mask);
	IRCInterface_PlaneRegisterOutMessage(msg);
	if (SendToServer(msg) < 0){
		printMsg("\nError, SendToServer failed @FunUwho.", LOG_ERR, 1);
		return -1;
	}
	printMsg("\nWHO sent to server.", LOG_INFO, 0);

	free(msg);
	if (mask)
		free(mask);

	return 0;
}
