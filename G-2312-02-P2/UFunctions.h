/**
 * @file UFunctions.h 
 * @brief Handel commands of user
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#ifndef UFUNCTIONS
#define UFUNCTIONS

#define DIM_PUFUNS 200

void addPUFunToArray(int pos, int (* fun)(char*));
void inicializatePUFuns(int (*def_fun)(char*));
int FunUDefault  (char* str);
int FunUnames  (char* str);
int FunUhelp   (char* str);
int FunUlist   (char* str);
int FunUjoin   (char* str);
int FunUpart   (char* str);
int FunUleave  (char* str);
int FunUquit   (char* str);
int FunUnick   (char* str);
int FunUaway   (char* str);
int FunUwhois  (char* str);
int FunUinvite (char* str);
int FunUkick   (char* str);
int FunUtopic  (char* str);
int FunUme     (char* str);
int FunUmsg    (char* str);
int FunUquery  (char* str);
int FunUnotice (char* str);
int FunUnotify (char* str);
int FunUignore (char* str);
int FunUping   (char* str);
int FunUwho    (char* str);

/*Array de punteros a funcion para las funciones de cada comando*/
int (*pfuns[DIM_PUFUNS])(char*);

#endif /* UFUNCTIONS */

