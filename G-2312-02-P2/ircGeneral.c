/**
 * @file ircGeneral.c
 * @brief defines constants and includes 

 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "ircGeneral.h"
#include "irc_auxFunctions.h"
#include "irc_client.h"

ssize_t SendToServer (char* msg){
	if(msg == NULL){
		return -1;
	}
	printf("MSG Send -> %s\n", msg);
	IRCInterface_PlaneRegisterOutMessage (msg);
	return write(cln.server_socket,msg,strlen(msg));
}

int socket_create(int type, int protocol, int port) {
    struct sockaddr_in servaddr;
    int descriptor;

    if ((descriptor = socket(type, protocol, 0)) == -1) {
        /*Cuando ejecutes con daemon no tienes printf, usa syslog*/
        printMsg("Error en socket\n", ERROR, 1);
        return -1;
    }

    bzero(&servaddr, sizeof (servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(port);

    if (bind(descriptor, (struct sockaddr*) &servaddr, sizeof (servaddr)) == -1) {
        printMsg("Error en bind\n", ERROR, 1);
        return -1;
    }

    if (listen(descriptor, 10) == -1) {
        printMsg("Error en listen\n", ERROR, 1);
        return -1;
    }

    return descriptor;
}
