/**
 * @file irc_auxFunctions.h 
 * @brief general axuiliar Funcions to help
 * @author Dario Adrian Hernandez
 * @author Miguel Angel Alvarez Rodriguez
 * @date 2017/4/12
 * @version 1.0
 */

#include "irc_auxFunctions.h"
#include "irc_client.h"

char * strclone(char * str){
    int dim;
    char * out;
    if(str == NULL){
        return NULL;
    }

    dim = strlen(str) + 1;
    if((out = (char *) calloc(sizeof(char), dim)) == NULL){
        return NULL;
    }

    strcpy(out, str);
    return out;
}

void printMsg(char *msg, int log_mode, int perr) {
    if (msg != NULL) {
    #ifdef DEBUG_PRINT
            if (perr) {
                perror(msg);
            } else {
                fprintf(stdout, "%s", msg);
            }
    #else
            syslog(log_mode, "%s", msg);
    #endif
    }
}

char* CharMalloc (){
    char* msg = (char*)malloc(MAXBYTES*sizeof(char));
    return msg;
}